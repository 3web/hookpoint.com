import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudyPierfrancesco from "../modules/caseStudy/caseStudyPierfrancesco.template"

const CaseStudyPierfrancescoPage = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Pierfrancesco Conte" />
      <CaseStudyPierfrancesco />
    </Layout>
  )
}

export default CaseStudyPierfrancescoPage
