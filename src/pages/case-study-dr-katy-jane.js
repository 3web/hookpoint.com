import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudy from "../modules/caseStudy/CaseStudyDrKatyJane.template"

const CaseStudyDrKatyJane = () => {
  return (
    <Layout>
      <SEO title="Case Study Dr. Katy Jane" />
      <CaseStudy />
    </Layout>
  )
}

export default CaseStudyDrKatyJane
