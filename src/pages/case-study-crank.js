import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudyCrankTemplate from "../modules/caseStudy/CaseStudyCrank.template"

const CaseStudyCrank = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Crank" />
      <CaseStudyCrankTemplate image={data.caseStudyCrank.edges} />
    </Layout>
  )
}

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }

    caseStudyCrank: allFile(
      filter: { relativeDirectory: { eq: "caseStudyCrank" } }
    ) {
      edges {
        node {
          id
          childImageSharp {
            fluid(maxWidth: 900, quality: 100) {
              ...GatsbyImageSharpFluid_noBase64
            }
          }
        }
      }
    }
  }
`

export default CaseStudyCrank
