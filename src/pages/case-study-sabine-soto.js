import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudyTemplate from "../modules/caseStudy/caseStudySabinteSoto.template"

const CaseStudyPage = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Sabine Soto" />
      <CaseStudyTemplate />
    </Layout>
  )
}

export default CaseStudyPage
