import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudy from "../modules/caseStudy/caseStudyRadha.template"

const CaseStudyRadha = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Radha" />
      <CaseStudy />
    </Layout>
  )
}

export default CaseStudyRadha
