import React from "react"
import { Link } from "gatsby"

import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import OurTeamContainer from "../modules/OurTeam/OurTeamContainer"

export default function OurTeam() {
  return (
    <Layout>
      <SEO title="Our team" />
      <OurTeamContainer />
    </Layout>
  )
}
