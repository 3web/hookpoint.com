import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudyRihannaTemplate from "../modules/caseStudy/CaseStudyRihanna.template"

const CaseStudyRihanna = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Rihanna" />
      <CaseStudyRihannaTemplate image={data.caseStudyRihanna.edges} />
    </Layout>
  )
}

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }

    caseStudyRihanna: allFile(
      filter: { relativeDirectory: { eq: "caseStudyRihanna" } }
    ) {
      edges {
        node {
          id
          childImageSharp {
            fluid(maxWidth: 900, quality: 100) {
              ...GatsbyImageSharpFluid_noBase64
            }
          }
        }
      }
    }
  }
`

export default CaseStudyRihanna
