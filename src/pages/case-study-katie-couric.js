import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudyKatieCouricTemplate from "../modules/caseStudy/CaseStudyKatieCouric.template"

const CaseStudyKatieCouric = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Katie Couric" />
      <CaseStudyKatieCouricTemplate image={data.caseStudyKatieCouric.edges} />
    </Layout>
  )
}

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }

    caseStudyKatieCouric: allFile(
      filter: { relativeDirectory: { eq: "caseStudyKatieCouric" } }
    ) {
      edges {
        node {
          id
          childImageSharp {
            fluid(maxWidth: 900, quality: 100) {
              ...GatsbyImageSharpFluid_noBase64
            }
          }
        }
      }
    }
  }
`

export default CaseStudyKatieCouric
