import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudyTemplate from "../modules/caseStudy/caseStudyGetMaineLobster.template"

const CaseStudyPage = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Get Maine Lobster" />
      <CaseStudyTemplate />
    </Layout>
  )
}

export default CaseStudyPage
