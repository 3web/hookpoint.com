import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import Logo from "../assets/images/logo.svg"
import { Script } from "gatsby"
function Apply() {
  return (
    <div>
      <div className="apply_logo">
        <a href="/">
          <img src={Logo} alt="hookpoint" />
        </a>
      </div>
      <SEO title="Apply" />
      <div className="apply_holder">
        <h1>Are you interested in working with Hook Point?</h1>
        <p>
          Let's connect! Will you please schedule a time below to speak with one
          of our team members?
        </p>
        <iframe
          width="100%"
          height="620"
          src="https://3web.bg/tmp/apply.html"
          title="hookpoint-apply"
        />
        {/* <div
          id="SOIDIV_HookPointDiscovery10"
          data-so-page="HookPointDiscovery10"
          data-height="550"
          data-style="border: 1px solid #d8d8d8; min-width: 290px; max-width: 900px;"
          data-psz="10"
        />
        <Script
          type="text/javascript"
          src="https://cdn.oncehub.com/mergedjs/so.js"
        /> */}
      </div>
    </div>
  )
}

export default Apply
