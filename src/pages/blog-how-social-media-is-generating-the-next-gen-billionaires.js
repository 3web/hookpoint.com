import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import BlogHowSocialMediaIsGeneratingTheNextGenBillionaires from "../modules/caseStudy/blogHowSocialMediaIsGeneratingTheNextGenBillionaires.template.js"

const CaseStudyPierfrancescoPage = ({ data }) => {
  return (
    <Layout>
      <SEO title="How social media is generating the next gen billionaires" />
      <BlogHowSocialMediaIsGeneratingTheNextGenBillionaires />
    </Layout>
  )
}

export default CaseStudyPierfrancescoPage
