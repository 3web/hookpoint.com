import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudy from "../modules/caseStudy/caseStudyAdan.template"

const CaseStudyAdan = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Cristian Adán" />
      <CaseStudy />
    </Layout>
  )
}

export default CaseStudyAdan
