import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not found" />
    <div
      style={{
        display: "flex",
        width: "100%",
        height: "100%",
        minHeight: "100vh",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        flexGrow: 1,
        position: "relative",
      }}
    >
      <div
        style={{
          position: "absolute",
          inset: 0,
          display: "flex",
          width: "100%",
          height: "100%",
          justifyContent: "center",
          alignItems: "center",
          fontSize: "300px",
          opacity: "0.1",
          fontWeight: "bolder",
          color: "#f00",
        }}
      >
        404
      </div>
      <h1 style={{ fontSize: "30px", marginBottom: "50px", zIndex: 1 }}>
        NOT FOUND
      </h1>
      <p style={{ zIndex: 1 }}>
        You just hit a route that doesn&#39;t exist... the sadness.
      </p>
    </div>
  </Layout>
)

export default NotFoundPage
