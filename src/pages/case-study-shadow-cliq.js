import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudy from "../modules/caseStudy/caseShadowCliq.template"

const CaseStudyPage = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Shadow Cliq" />
      <CaseStudy />
    </Layout>
  )
}

export default CaseStudyPage
