import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudySahasChopra from "../modules/caseStudy/caseStudySahasChopra.template"

const CaseStudyPierfrancescoPage = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Sahas Chopra" />
      <CaseStudySahasChopra />
    </Layout>
  )
}

export default CaseStudyPierfrancescoPage
