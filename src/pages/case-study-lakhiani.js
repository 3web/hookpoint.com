import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudyLakhianiTemplate from "../modules/caseStudy/CaseStudyLakhiani.template"

const CaseStudyLakhiani = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study - Vishen Lakhiani" />
      <CaseStudyLakhianiTemplate image={data.caseStudyLakhiani.edges} />
    </Layout>
  )
}

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }

    caseStudyLakhiani: allFile(
      filter: { relativeDirectory: { eq: "caseStudyLakhiani" } }
    ) {
      edges {
        node {
          id
          childImageSharp {
            fluid(maxWidth: 900, quality: 100) {
              ...GatsbyImageSharpFluid_noBase64
            }
          }
        }
      }
    }
  }
`

export default CaseStudyLakhiani
