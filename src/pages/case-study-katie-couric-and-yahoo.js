import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudyTemplate from "../modules/caseStudy/caseStudyKatieCouricAndYahoo.template"

const CaseStudyPage = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Katie Couric and Yahoo!" />
      <CaseStudyTemplate />
    </Layout>
  )
}

export default CaseStudyPage
