import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudyMindValleyTemplate from "../modules/caseStudy/CaseStudyMindValley.template"

const CaseStudyMindValley = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Mindvalley" />
      <CaseStudyMindValleyTemplate image={data.caseStudyMindValley.edges} />
    </Layout>
  )
}

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }

    caseStudyMindValley: allFile(
      filter: { relativeDirectory: { eq: "caseStudyMindValley" } }
    ) {
      edges {
        node {
          id
          childImageSharp {
            fluid(maxWidth: 900, quality: 100) {
              ...GatsbyImageSharpFluid_noBase64
            }
          }
        }
      }
    }
  }
`

export default CaseStudyMindValley
