import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudyYahooTemplate from "../modules/caseStudy/CaseStudyYahoo.template"

const CaseStudyYahoo = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Yahoo" />
      <CaseStudyYahooTemplate image={data.caseStudyYahoo.edges} />
    </Layout>
  )
}

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }

    caseStudyYahoo: allFile(
      filter: { relativeDirectory: { eq: "caseStudyYahoo" } }
    ) {
      edges {
        node {
          id
          childImageSharp {
            fluid(maxWidth: 900, quality: 100) {
              ...GatsbyImageSharpFluid_noBase64
            }
          }
        }
      }
    }
  }
`

export default CaseStudyYahoo
