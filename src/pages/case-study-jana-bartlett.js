import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudyJanaTemplate from "../modules/caseStudy/CaseStudyJana.template"

const CaseStudyDrStaci = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study - Jana Bartlett" />
      <CaseStudyJanaTemplate image={data.caseStudyDrStaci.edges} />
    </Layout>
  )
}

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }

    caseStudyDrStaci: allFile(
      filter: { relativeDirectory: { eq: "caseStudyJana" } }
    ) {
      edges {
        node {
          id
          childImageSharp {
            fluid(maxWidth: 900, quality: 100) {
              ...GatsbyImageSharpFluid_noBase64
            }
          }
        }
      }
    }
  }
`

export default CaseStudyDrStaci
