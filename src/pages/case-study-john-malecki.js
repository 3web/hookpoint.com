import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudyTemplate from "../modules/caseStudy/CaseStudyJohnMalecki.template"

const CaseStudyPage = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study John Malecki" />
      <CaseStudyTemplate />
    </Layout>
  )
}

export default CaseStudyPage
