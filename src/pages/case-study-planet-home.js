import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudyPlanetHomeTemplate from "../modules/caseStudy/CaseStudyPlanetHome.template"

const CaseStudyPlanetHome = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Planet Home" />
      <CaseStudyPlanetHomeTemplate image={data.caseStudyPlanetHome.edges} />
    </Layout>
  )
}

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }

    caseStudyPlanetHome: allFile(
      filter: { relativeDirectory: { eq: "caseStudyPlanetHome" } }
    ) {
      edges {
        node {
          id
          childImageSharp {
            fluid(maxWidth: 900, quality: 100) {
              ...GatsbyImageSharpFluid_noBase64
            }
          }
        }
      }
    }
  }
`

export default CaseStudyPlanetHome
