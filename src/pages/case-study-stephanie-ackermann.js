import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudy from "../modules/caseStudy/caseStudyStephanieAckermann.template"

const CaseStudyStephanieAckermann = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Stephanie Ackermann" />
      <CaseStudy />
    </Layout>
  )
}

export default CaseStudyStephanieAckermann
