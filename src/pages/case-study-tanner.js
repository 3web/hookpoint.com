import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudyTanner from "../modules/caseStudy/caseStudyTanner.template"

const CaseStudyTannerPage = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Tanner Leatherstein" />
      <CaseStudyTanner />
    </Layout>
  )
}

export default CaseStudyTannerPage
