import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudyRogerTemplate from "../modules/caseStudy/caseStudyRoger.template"

const CaseStudyDrStaci = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study - Roger Wakefield" />
      <CaseStudyRogerTemplate image={data.caseStudyRoger.edges} />
    </Layout>
  )
}

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }

    caseStudyRoger: allFile(
      filter: { relativeDirectory: { eq: "caseStudyRoger" } }
    ) {
      edges {
        node {
          id
          childImageSharp {
            fluid(maxWidth: 900, quality: 100) {
              ...GatsbyImageSharpFluid_noBase64
            }
          }
        }
      }
    }
  }
`

export default CaseStudyDrStaci
