import React from "react"
import Layout from "../common/layout/layout"
import SEO from "../common/seo/seo"
import CaseStudyTemplate from "../modules/caseStudy/caseStudyVanessaBlasic.template"

const CaseStudyPage = ({ data }) => {
  return (
    <Layout>
      <SEO title="Case Study Vanessa Blasic" />
      <CaseStudyTemplate />
    </Layout>
  )
}

export default CaseStudyPage
