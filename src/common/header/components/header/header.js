import { Link } from "gatsby"
import HeaderStyle from "./HeaderStyle.module.styl"
import React from "react"
import Logo from "../../../../assets/images/logo.svg"

const Header = () => {
  return (
    <header className={HeaderStyle.header}>
      <div className={HeaderStyle.header_content}>
        <Link to="/home" className={HeaderStyle.logo}>
          <img src={Logo} alt="logo" />
        </Link>
        <div className={HeaderStyle.menu}>
          <Link
            to="/about"
            className={HeaderStyle.link}
            activeClassName={HeaderStyle.active_link}
          >
            About
          </Link>
          {/* <Link
            to="/services"
            className={HeaderStyle.link}
            activeClassName={HeaderStyle.active_link}
          >
            Services
          </Link> */}
          <Link
            to="/case-studies"
            className={HeaderStyle.link}
            activeClassName={HeaderStyle.active_link}
          >
            Case Studies
          </Link>
          <Link
            to="/the-book"
            className={HeaderStyle.link}
            activeClassName={HeaderStyle.active_link}
          >
            The Book
          </Link>
          {/* <a
            target="_blank"
            rel="noreferrer"
            href="https://masterclass.hookpoint.com/"
            className={HeaderStyle.link}
          >
            Masterclass
          </a> */}
          <Link
            target="_blank"
            rel="noreferrer"
            to="/our-team"
            className={HeaderStyle.link}
            activeClassName={HeaderStyle.active_link}
          >
            Our Team
          </Link>
          <a
            target="_blank"
            rel="noreferrer"
            href="https://brendanjkane.com/bkblog/"
            className={HeaderStyle.link}
          >
            Blog
          </a>
          <Link
            target="_blank"
            rel="noreferrer"
            to="/apply"
            className={HeaderStyle.link}
          >
            Work With Us
          </Link>
        </div>
      </div>
    </header>
  )
}
export default Header
