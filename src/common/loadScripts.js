import React, { useEffect } from "react"

/**
 *
 * @param {Array<string>} sources
 */
export function loadScripts(sources) {
  if (sources.length) {
    for (let i = 0; i < sources.length; i++) {
      const source = sources[i]
      if (source) {
        /**
         * @type {HTMLElement}
         */
        const script = document.createElement("script")
        script.src = source
        document.body.appendChild(script)
      }
    }
  }
}

/**
 *
 * @param {Array<string>} sources
 */
export default function useLoadScripts(sources) {
  useEffect(() => {
    loadScripts(sources)
  }, [sources])
  return null
}
