import React from "react"
import ApplyToWorkSectionStyle from "./ApplyToWorkSectionStyle.module.styl"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faAngleRight } from "@fortawesome/free-solid-svg-icons"

const ApplyToWorkSection = props => {
  return (
    <section className={ApplyToWorkSectionStyle.apply_to_work_section}>
      <div className={ApplyToWorkSectionStyle.content_holder}>
        <h2>
          Apply to work with us<span>.</span>
        </h2>
        <div className={ApplyToWorkSectionStyle.apply_btn}>
          Apply Now <FontAwesomeIcon icon={faAngleRight} />
        </div>
      </div>
    </section>
  )
}

export default ApplyToWorkSection
