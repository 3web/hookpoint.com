import React, { useState } from "react"
import UniqueAboutYouSectionStyle from "./UniqueAboutYouSectionStyle.module.styl"
import Footer from "../../../../common/footer/Footer"
import Cross from "../../../../assets/images/cross.svg"

const charPoses = {
  exit: { opacity: 0, y: 0 },
  enter: {
    opacity: 1,
    y: 0,
    delay: ({ charIndex }) => charIndex * 30,
  },
}

function UniqueAboutYouSection() {
  const [isVisible, setSectionVisible] = useState(false)

  function onChange() {
    setSectionVisible(!isVisible)
  }
  return (
    <section className={UniqueAboutYouSectionStyle.unique_about_you_section}>
      <div className={UniqueAboutYouSectionStyle.unique_about_you}>
        <div className={UniqueAboutYouSectionStyle.unique_about_you_content}>
          <h2>
            We find what’s
            <span>unique</span> about your brand and help you deliver it at
            scale.
          </h2>

          <div className={UniqueAboutYouSectionStyle.content}>
            No templates, no one-size-fits-all approaches. Unexpected solutions
            your
            <br /> customers are delighted to see.
          </div>
        </div>
        <img
          className={UniqueAboutYouSectionStyle.cross}
          src={Cross}
          alt="cross"
        />
      </div>
      <Footer />
    </section>
  )
}

export default UniqueAboutYouSection
