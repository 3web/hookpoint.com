import React, { useState } from "react"
import AboutHeroSectionStyle from "./AboutHeroSectionStyle.module.styl"
import Footer from "../../../../common/footer/Footer"
import Cross from "../../../../assets/images/cross.svg"


const charPoses = {
  exit: { opacity: 0, y: 0 },
  enter: {
    opacity: 1,
    y: 0,
    delay: ({ charIndex }) => charIndex * 30,
  },
}

function AboutHeroSection() {
  const [isVisible, setSectionVisible] = useState(false)

  function onChange() {
    setSectionVisible(!isVisible)
  }
  return (
    <section className={AboutHeroSectionStyle.about_hero_section}>
      <div className={AboutHeroSectionStyle.make_your_business_section}>
        <div className={AboutHeroSectionStyle.make_yout_business}>
          <h2>It takes 3 seconds to make or break your business.</h2>

          <h3>
            We help your brand stand out in crowded markets. There are over 60
            billion messages on digital platforms everyday. The average person
            is exposed to 4,000 to 10,000 ads every 24 hours. Inundated with
            choices, today’s consumers make decisions in milliseconds.
          </h3>

          <img
            className={AboutHeroSectionStyle.cross}
            src={Cross}
            alt="cross"
          />
        </div>
      </div>
      <Footer />
    </section>
  )
}

export default AboutHeroSection
