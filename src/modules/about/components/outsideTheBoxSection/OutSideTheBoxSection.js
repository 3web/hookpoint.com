import React, { useState } from "react"
import OutSideTheBoxSectionStyle from "./OutsideTheBoxSectionStyle.module.styl"
import Footer from "../../../../common/footer/Footer"
import Cross from "../../../../assets/images/cross.svg"

const charPoses = {
  exit: { opacity: 0, y: 0 },
  enter: {
    opacity: 1,
    y: 0,
    delay: ({ charIndex }) => charIndex * 30,
  },
}

function OutSideTheBoxSection() {
  const [isVisible, setSectionVisible] = useState(false)

  function onChange() {
    setSectionVisible(!isVisible)
  }
  return (
    <section className={OutSideTheBoxSectionStyle.outside_the_box_section}>
      <div className={OutSideTheBoxSectionStyle.outisde_the_box_text}>
        <h2>
          We’ve helped thousands of people to use Hook Points to reach billions
          of people around the world across multiple industries.
        </h2>

        <div className={OutSideTheBoxSectionStyle.content}>
          Outside the box thinkers that drive brands, corporations and
          celebrities in highly crowded, competitive markets. Most emphasize on
          maximizing attention. We command it.
        </div>

        <img
          className={OutSideTheBoxSectionStyle.cross}
          src={Cross}
          alt="cross"
        />
      </div>
      <Footer />
    </section>
  )
}

export default OutSideTheBoxSection
