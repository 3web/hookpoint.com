import React from "react"
import NewsletterSection from "../newsletter/NewsletterSection"
import ServicesSection from "./components/servicesSection/ServicesSection"
import UniqueAboutYouSection from "./components/uniqueAboutYouSection/UniqueAboutYouSection"
import OutSideTheBoxSection from "./components/outsideTheBoxSection/OutSideTheBoxSection"
import QuestionSection from "./components/questionSection/QuestionSection"
import AboutHeroSection from "./components/heroSection/AboutHeroSection"
import WorkSection from "../caseStudies/components/workSection/WorkSection"

const AboutPageTemplate = ({
  uniqueSectionImage,
  questionSectionImage,
  aboutHeroSectionImage,
}) => {
  return (
    <>
      <AboutHeroSection aboutHeroSectionImage={aboutHeroSectionImage} />
      <QuestionSection questionSectionImage={questionSectionImage} />
      <OutSideTheBoxSection />
      <UniqueAboutYouSection uniqueSectionImage={uniqueSectionImage} />
      <ServicesSection />
      <WorkSection />
      <NewsletterSection/>
    </>
  )
}

export default AboutPageTemplate
