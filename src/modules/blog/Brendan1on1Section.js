import React from "react"
import s from "./Brendan1on1.module.styl"
import bk from "../../assets/images/blogs/brendan-kane-1/brendan-kane-1@2x.png"
export default function Brendan1on1Section() {
  return (
    <section className={s.section}>
      <div className={s.container}>
        <img src={bk} className={s.avatar} />
        <article className={s.text}>
          <p>Want help with your social media strategy?</p>{" "}
          <p>
            Connect 1-on-1 with Brendan's team{" "}
            <a href="https://brendanjkane.com/work-with-brendan3/">here</a> to
            see if we are the right fit to work together.
          </p>
        </article>
      </div>
    </section>
  )
}
