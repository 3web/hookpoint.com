import React from "react"
import NewsletterSectionStyle from "./NewsletterSectionStyle.module.styl"
import { faAngleRight } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Video from "../../assets/images/Clouds.mp4"
import { config } from "@fortawesome/fontawesome-svg-core"
import "@fortawesome/fontawesome-svg-core/styles.css"
import VisibilitySensor from "react-visibility-sensor"
import SplitText from "react-pose-text"
import isValidEmail from "./../../common/emailValidator/EmailValidator"

config.autoAddCss = false

class NewsletterSection extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isVisible: false,
      error: "",
      inf_field_Email: "",
    }
  }
  onChange = () => {
    this.setState({ isVisible: !this.state.isVisible })
  }
  handleInput = e => {
    this.setState({
      [e.target.name]: e.target.value,
    })
  }
  submitForm = () => {
    const { inf_field_Email } = this.state

    if (!isValidEmail(inf_field_Email)) {
      this.setState({error: "Wrong email address !"})
      return
    }
    document
      .getElementById("inf_form_5b3ed8a192f0ed69fafda39fce1a8401")
      .submit()
  }
  render() {
    const charPoses = {
      exit: { opacity: 0, y: 0 },
      enter: {
        opacity: 1,
        y: 0,
        delay: ({ charIndex }) => charIndex * 50,
      },
    }
    const { isVisible, error, inf_field_Email } = this.state

    return (
      <VisibilitySensor
        minTopValue={200}
        partialVisibility={true}
        onChange={this.onChange}
      >
        <section className={NewsletterSectionStyle.newsletter_section}>
          <video
            className={NewsletterSectionStyle.image}
            autoPlay
            loop
            muted
            playsInline
          >
            <source src={Video} type="video/mp4" />
          </video>

          <div className={NewsletterSectionStyle.content_holder}>
            {isVisible === false ? (
              <h2>
                <SplitText
                  initialPose="exit"
                  pose={"enter"}
                  charPoses={charPoses}
                >
                  Stand out.
                </SplitText>
              </h2>
            ) : (
              <h2>Stand out.</h2>
            )}
            {isVisible === false ? (
              <h3>
                <SplitText
                  initialPose="exit"
                  pose={"enter"}
                  charPoses={charPoses}
                >
                  Subscribe for our newsletter
                </SplitText>
              </h3>
            ) : (
              <h3>Subscribe for our newsletter</h3>
            )}
            <form
              accept-charset="UTF-8"
              action="https://zp489.infusionsoft.com/app/form/process/5b3ed8a192f0ed69fafda39fce1a8401"
              className="infusion-form"
              id="inf_form_5b3ed8a192f0ed69fafda39fce1a8401"
              method="POST"
            >
              <input
                name="inf_form_xid"
                type="hidden"
                value="5b3ed8a192f0ed69fafda39fce1a8401"
              />
              <input
                name="inf_form_name"
                type="hidden"
                value="Newsletter HookPoint"
              />
              <input
                name="infusionsoft_version"
                type="hidden"
                value="1.70.0.262866"
              />
              <div className="infusion-field">
                <input
                  required
                  className="infusion-field-input"
                  id="inf_field_Email"
                  name="inf_field_Email"
                  placeholder="Your Email"
                  type="email"
                  onChange={this.handleInput}
                  value={inf_field_Email}
                />
              </div>
              <div className="infusion-submit">
                <button onClick={this.submitForm} type="submit">
                  <FontAwesomeIcon icon={faAngleRight} />
                </button>
              </div>
            </form>
            {error && <p className={NewsletterSectionStyle.error}>{error}</p>}
            <script
              type="text/javascript"
              src="https://zp489.infusionsoft.app/app/webTracking/getTrackingCode"
            />
            <script
              type="text/javascript"
              src="https://zp489.infusionsoft.com/app/timezone/timezoneInputJs?xid=5b3ed8a192f0ed69fafda39fce1a8401"
            />
          </div>
        </section>
      </VisibilitySensor>
    )
  }
}

export default NewsletterSection
