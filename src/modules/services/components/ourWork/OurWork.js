import React from "react"
import OurWorkStyle from "./OurWorkStyle.module.styl"
import Footer from "../../../../common/footer/Footer"

const OurWork = () => {
  return (
    <section className={OurWorkStyle.section}>
      <div className={OurWorkStyle.content_wrapper}>
        <h3>
          View our work
          <div className={OurWorkStyle.line} />
        </h3>
        <div className={OurWorkStyle.content}>
          Client case studies
        </div>
      </div>
      <Footer color={false} />
    </section>
  )
}

export default OurWork
