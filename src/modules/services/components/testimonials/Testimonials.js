import React from "react"
import TestimonialsStyle from "./TestimonialsStyle.module.styl"
import Footer from "../../../../common/footer/Footer"

const Testimonials = () => {
  return (
    <section className={TestimonialsStyle.section}>
      <div className={TestimonialsStyle.content_wrapper}>
        <h3>
          Testimonials
          <div className={TestimonialsStyle.line} />
        </h3>
        <div className={TestimonialsStyle.content}>
          <p>
            Read what people say <span>about working with us:</span>
          </p>
        </div>
      </div>
      <Footer color={false} />
    </section>
  )
}

export default Testimonials
