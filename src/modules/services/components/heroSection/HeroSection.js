import React from "react"
import HeroSectionStyle from "./HeroSectionStyle.module.styl"
import Footer from "../../../../common/footer/Footer"

const HeroSection = () => {
  return (
    <section className={HeroSectionStyle.wrapper}>
      <div className={HeroSectionStyle.case_studies_holder}>
        <h2>
          Services
          <span className={HeroSectionStyle.hr} />
        </h2>

        <div className={HeroSectionStyle.content}>
          Hook Point helps brands beat out competition by grabbing attention and
          standing out in the overcrowded market we find ourselves in today.
        </div>
      </div>
      <div className={HeroSectionStyle.image_holder} />
      <Footer />
    </section>
  )
}

export default HeroSection
