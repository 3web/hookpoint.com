import React from "react"
import HookPointsOffersStyle from "./HookPointsOffersStyle.module.styl"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons/faCheckCircle"
import { Link } from "gatsby"

const HookPointsOffers = () => {
  return (
    <section className={HookPointsOffersStyle.section}>
      <div className={HookPointsOffersStyle.content}>
        <h2 className={HookPointsOffersStyle.section_title}>
          Hook Point offers:
        </h2>
        <div className={HookPointsOffersStyle.offers_holder}>
          <div className={HookPointsOffersStyle.offer}>
            <div className={HookPointsOffersStyle.offer_header}>
              <h3>Hook Point Jump Start</h3>
            </div>
            <div className={HookPointsOffersStyle.offer_body}>
              <div className={HookPointsOffersStyle.offer_body_content}>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} />
                  Hook Point strategy
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} />
                  Qualify for ongoing Hook Point One-On-One program
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} />2 Hook Point strategy
                  calls
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} />
                  Work directly with the Hook Point team (Brendan will not be on
                  calls but will still oversee the creative)
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} />
                  Qualify for ongoing Hook Point One-On-One Program
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} />1 Guaranteed Hook
                  Point done for you
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} />
                  Delivered presentation with your Hook Point{" "}
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} />
                  High level go to market strategy for your Hook Point
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} />
                  Discounted pricing ongoing support
                </p>
                <a
                  className={HookPointsOffersStyle.link}
                  href="https://hookpoint.com/apply/"
                >
                  Apply Now
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="5.848"
                    height="9.647"
                    viewBox="0 0 5.848 9.647"
                  >
                    <path
                      className="a"
                      d="M4.817,3.5,1.179,0,0,1.229,4.817,5.848l4.83-4.618L8.467,0Z"
                      transform="translate(0 9.647) rotate(-90)"
                    />
                  </svg>
                </a>
              </div>
            </div>
          </div>
          <div className={HookPointsOffersStyle.offer}>
            <div className={HookPointsOffersStyle.offer_header_two}>
              <h3>Hook Point Premiere</h3>
            </div>
            <div className={HookPointsOffersStyle.offer_body}>
              <div className={HookPointsOffersStyle.offer_body_content}>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} />
                  Hook Point strategy
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} />
                  Qualify for ongoing Hook Point One-On-One program
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} /> 3 Hook Point strategy
                  calls
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} />
                  Guaranteed calls with Brendan Kane
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} />
                  Qualify for ongoing Hook Point One-On-One Program
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} /> 3 Guaranteed Hook
                  Points done for you
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} />
                  Delivered presentation with your Hook Points
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} />
                  High level go to market strategy for your Hook Points
                </p>
                <p>
                  <FontAwesomeIcon icon={faCheckCircle} />
                  Discounted pricing ongoing support
                </p>
                <a
                  style={{ marginTop: 64 }}
                  className={HookPointsOffersStyle.link}
                  href="https://hookpoint.com/apply/"
                >
                  Apply Now
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="5.848"
                    height="9.647"
                    viewBox="0 0 5.848 9.647"
                  >
                    <path
                      className="a"
                      d="M4.817,3.5,1.179,0,0,1.229,4.817,5.848l4.83-4.618L8.467,0Z"
                      transform="translate(0 9.647) rotate(-90)"
                    />
                  </svg>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default HookPointsOffers
