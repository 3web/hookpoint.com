import React from "react"
import ReviewSectionStyle from "./ClientsStyle.module.styl"

const Clients = ({ children }) => {
  return (
    <section className={ReviewSectionStyle.review_section}>{children}</section>
  )
}

export default Clients
