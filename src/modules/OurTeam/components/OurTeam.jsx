import React from "react"
import { items } from "../constants/index"
import s from "../styles/ourTeam.module.styl"

const OurTeam = ({}) => {
  return (
    <div className={`${s.background}`}>
      <section className={`${s.section} ${s.container}`}>
        <div className={s.grid}>
          {items?.map(item => {
            return (
              <div key={item?.name} className={s.item}>
                <img src={item.img} />
                <h3>{item.name}</h3>
                <p>{item.position}</p>
              </div>
            )
          })}
        </div>
      </section>
    </div>
  )
}

export default OurTeam
