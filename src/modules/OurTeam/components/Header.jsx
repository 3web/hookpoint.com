import React from "react"
import s from "../styles/ourTeam.module.styl"

export default function Header() {
  return (
    <header className={`${s.container} ${s.header}`}>
      <h1 className={s.title}>Our Team</h1>
      <div className={s.line} />
      <h3 className={s.subtitle}>
        We’re about people. We put human beings at the forefront of everything
        we do, ensuring our ideas and plans are culturally rich and
        consumer-led.
      </h3>
    </header>
  )
}
