import React from "react"
import Header from "./components/Header"
import OurTeam from "./components/OurTeam"

const OurTeamContainer = ({}) => {
  return (
    <React.Fragment>
      <Header />
      <OurTeam />
    </React.Fragment>
  )
}

export default OurTeamContainer
