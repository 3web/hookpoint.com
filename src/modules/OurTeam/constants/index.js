import BrendanKane from "../../../assets/images/ourTeam/Brendan/Brendan.png"
import DaveCohen from "../../../assets/images/ourTeam/Dave/Dave.png"
import JeffKing from "../../../assets/images/ourTeam/Jeff/Jeff.png"
import JoseBrizida from "../../../assets/images/ourTeam/Jose/Jose.png"
import KatyaGuseva from "../../../assets/images/ourTeam/Katya/Katya.png"
import KyleTso from "../../../assets/images/ourTeam/Kyle/Kyle.png"
import LukeEvenson from "../../../assets/images/ourTeam/Luke/Luke.png"
import MelissaHuisman from "../../../assets/images/ourTeam/Melissa/Melissa.png"
import MichaelGlines from "../../../assets/images/ourTeam/Michael/Michael.png"
import Naveen from "../../../assets/images/ourTeam/naveen/naveen.png"
import PauloDeSouse from "../../../assets/images/ourTeam/Paulo/Paulo.png"
import Rhode from "../../../assets/images/ourTeam/Rhode/Rhode.png"
import SalomeLobo from "../../../assets/images/ourTeam/Salome/Salome.png"
import SamanthaJaneBee from "../../../assets/images/ourTeam/Samantha/Samantha.png"
import Sharon from "../../../assets/images/ourTeam/Sharon/Sharon.png"
import Marco from "../../../assets/images/ourTeam/Marco.png"
import John from "../../../assets/images/ourTeam/John.png"
//15.05.23
import Ana_Gilbert from "../../../assets/images/ourTeam/Ana_Gilbert/Ana_Gilbert@2x.png"
import Andres_Ospina from "../../../assets/images/ourTeam/Andres_Ospina/Andres_Ospina@2x.png"
import Austin_Riley from "../../../assets/images/ourTeam/Austin_Riley/Austin_Riley@2x.png"
import Danielle_Goldenberg from "../../../assets/images/ourTeam/Danielle_Goldenberg/Danielle_Goldenberg@2x.png"
import David_Joseph from "../../../assets/images/ourTeam/David_Joseph/David_Joseph@2x.png"
import Derrien_King from "../../../assets/images/ourTeam/Derrien_King/Derrien_King@2x.png"
import Ivan_Nikolov from "../../../assets/images/ourTeam/Ivan_Nikolov/Ivan_Nikolov@2x.png"
import Jasna_Hodzic from "../../../assets/images/ourTeam/Jasna_Hodzic/Jasna_Hodzic@2x.png"
import Leonor_Rebelo from "../../../assets/images/ourTeam/Leonor_Rebelo/Leonor_Rebelo@2x.png"
import Tom_Byrappa from "../../../assets/images/ourTeam/Tom_Byrappa/Tom_Byrappa@2x.png"
import Zack_Pinkney from "../../../assets/images/ourTeam/Zack_Pinkney/Zack_Pinkney@2x.png"

export const OurTeamConstants = { __example: null }
export const items = [
  {
    img: BrendanKane,
    name: "Brendan Kane",
    position: "Founder and CEO",
  },
  { img: JeffKing, name: "Jeff King", position: "President" },
  {
    img: PauloDeSouse,
    name: "Paulo de Sousa",
    position: "Chief Administration Officer",
  },
  { img: DaveCohen, name: "Dave Cohen", position: "Financial Advisor" },
  { img: Naveen, name: "Naveen Gouda", position: "Creative Director" },
  {
    img: JoseBrizida,
    name: "José Brizida",
    position: "Marketing Director",
  },
  {
    img: Sharon,
    name: "Sharon Hunt",
    position: "Online Business Manager",
  },
  {
    img: SamanthaJaneBee,
    name: "Samantha Jane Bee",
    position: "Client Services Manager",
  },
  {
    img: MelissaHuisman,
    name: "Melissa Huisman",
    position: "Lead Writer / Project Manager",
  },
  {
    img: LukeEvenson,
    name: "Luke Evenson",
    position: "Senior Creative Specialist",
  },
  {
    img: KatyaGuseva,
    name: "Katya Guseva",
    position: "Senior Creative Specialist",
  },
  {
    img: MichaelGlines,
    name: "Michael Glines",
    position: "Creative Specialist",
  },
  //15.05.2023
  {
    img: Austin_Riley,
    name: "Austin Riley",
    position: "Editor & Creative Specialist",
  },
  {
    img: Derrien_King,
    name: "Derrien King",
    position: "Communications Specialist",
  },
  {
    img: Leonor_Rebelo,
    name: "Leonor Rebelo",
    position: "Executive Assistant",
  },
  {
    img: Ivan_Nikolov,
    name: "Ivan Nikolov",
    position: "Internal Growth Strategist",
  },
  {
    img: Danielle_Goldenberg,
    name: "Danielle Goldenberg",
    position: "Marketing Assistant",
  },
  {
    img: Ana_Gilbert,
    name: "Ana Gilbert",
    position: "Project Manager",
  },
  {
    img: Jasna_Hodzic,
    name: "Jasna Hodzic",
    position: "Analyst",
  },
  {
    img: David_Joseph,
    name: "David Joseph",
    position: "Director Of Enrollment",
  },
  {
    img: Andres_Ospina,
    name: "Andreas Ospina",
    position: "Director Of Enrollment",
  },
  {
    img: Tom_Byrappa,
    name: "Tom Byrappa",
    position: "Director Of Enrollment",
  },
  {
    img: Zack_Pinkney,
    name: "Zack Pinkney",
    position: "Director Of Enrollment",
  },
]
