import React from "react"
import CaseStudiesImagesStyle from "./CaseStudiesImagesStyle.module.styl"
import { Link } from "gatsby"
import Image from "gatsby-image"
import pier from "../../../../assets/images/caseStudyPierfrancesco/thumb.jpeg"
import sahas from "../../../../assets/images/caseStudySahasChopra/thumbnail.png"
import ackermann from "../../../../assets/images/caseStudyStephanieAckermann/Group3115/Group3115.png"
import rhada from "../../../../assets/images/caseStudyRadha/Group3116/Group3116.png"
import christianadan from "../../../../assets/images/christianadan/square.png"
import tanner from "../../../../assets/images/caseStudyTanner/square/square2.png"
import katyjane from "../../../../assets/images/CaseStudyDrKatyJane/square.png"
import liia from "../../../../assets/images/CaseStudyDrLiiaRamachandra/square.png"
import erinNance from "../../../../assets/images/erinNance/square/Group 3125@2x.png"
import shadowCliq from "../../../../assets/images/shadowCliq/square/Group 3126@2x.png"
import ts from "../../../../assets/images/ts/TS2.png"
//19.05.2023
import katie_couric_and_yahoo from "../../../../assets/images/katie_couric_and_yahoo/square/s2.png"
import vanessa_blasic from "../../../../assets/images/vanessa_blasic/square/s2.png"
import sabine_soto from "../../../../assets/images/sabine_soto/square/s2.png"
import get_maine_lobster from "../../../../assets/images/get_maine_lobster/square/s2.png"
import john_malecki from "../../../../assets/images/john_malecki/square.webp"

const CaseStudiesImages = ({
  caseStudyImage1,
  caseStudyImage2,
  caseStudyImage3,
  caseStudyImage4,
  caseStudyImage5,
  caseStudyImage6,
  caseStudyImage7,
  caseStudyImage8,
  caseStudyImage9,
  caseStudyImage10,
  caseStudyImage11,
  caseStudyImage12,
  caseStudyImage13,
  caseStudyImage14,
  caseStudyImage15,
  caseStudyImage16,
  caseStudyImage17,
  caseStudyImage18,
}) => {
  const img1 = caseStudyImage1[0]?.node.childImageSharp.fluid
  const img2 = caseStudyImage2[0]?.node.childImageSharp.fluid
  const img3 = caseStudyImage3[0]?.node.childImageSharp.fluid
  const img4 = caseStudyImage4[0]?.node.childImageSharp.fluid
  const img5 = caseStudyImage5[0]?.node.childImageSharp.fluid
  const img6 = caseStudyImage6[0]?.node.childImageSharp.fluid
  const img7 = caseStudyImage7[0]?.node.childImageSharp.fluid
  const img8 = caseStudyImage8[0]?.node.childImageSharp.fluid
  const img9 = caseStudyImage9[0]?.node.childImageSharp.fluid
  const img10 = caseStudyImage10[0]?.node.childImageSharp.fluid
  const img11 = caseStudyImage11[0]?.node.childImageSharp.fluid
  const img12 = caseStudyImage12[0]?.node.childImageSharp.fluid
  const img13 = caseStudyImage13[0]?.node.childImageSharp.fluid
  const img14 = caseStudyImage14[0]?.node.childImageSharp.fluid
  const img15 =
    caseStudyImage15 && caseStudyImage15[1]?.node?.childImageSharp?.fluid
  const img16 =
    caseStudyImage16 && caseStudyImage16[1]?.node?.childImageSharp?.fluid
  const img17 =
    caseStudyImage17 && caseStudyImage17[1]?.node?.childImageSharp?.fluid
  const img18 =
    caseStudyImage18 && caseStudyImage18[1]?.node?.childImageSharp?.fluid
  return (
    <section className={CaseStudiesImagesStyle.case_studies_images_section}>
      <div className="row no-gutters">
        <div className="col-lg-4 col-sm-6">
          <Link
            to="/case-study-tylor-swift"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image
              className={CaseStudiesImagesStyle.image}
              fluid={{
                aspectRatio: 0.907258064516129,
                sizes: "(max-width: 537px) 100vw, 537px",
                src: ts,
                srcSet: ts,
              }}
            />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-lakhiani"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img18} />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-rihanna"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img3} />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-crank"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img7} />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-nordic-naturals"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img4} />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          <Link
            to="/case-study-tanner"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image
              className={CaseStudiesImagesStyle.image}
              fluid={{
                aspectRatio: 0.907258064516129,
                sizes: "(max-width: 537px) 100vw, 537px",
                src: tanner,
                srcSet: tanner,
              }}
            />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-strike-social"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img8} />
          </Link>
        </div>{" "}
        {/* <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-yahoo"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img5} />
          </Link>
        </div> */}
        <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-mtv"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img2} />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-jana-bartlett"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img16} />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-mind-valley"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img6} />
          </Link>
        </div>
        {/* <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-katie-couric"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img10} />
          </Link>
        </div>{" "} */}
        <div className="col-lg-4 col-sm-6">
          <Link
            to="/case-study-sahas"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image
              className={CaseStudiesImagesStyle.image}
              fluid={{
                aspectRatio: 0.907258064516129,
                sizes: "(max-width: 537px) 100vw, 537px",
                src: sahas,
                srcSet: sahas,
              }}
            />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-lala-anthony"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img9} />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-michael-breus"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img11} />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-craig-clemens"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img14} />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-one-million-followers"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img12} />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-pierfrancesco"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image
              className={CaseStudiesImagesStyle.image}
              fluid={{
                aspectRatio: 0.907258064516129,
                sizes: "(max-width: 537px) 100vw, 537px",
                src: pier,
                srcSet: pier,
              }}
            />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          <Link
            to="/case-study-stephanie-ackermann"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image
              className={CaseStudiesImagesStyle.image}
              fluid={{
                aspectRatio: 0.907258064516129,
                sizes: "(max-width: 537px) 100vw, 537px",
                src: ackermann,
                srcSet: ackermann,
              }}
            />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-planet-home"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img13} />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-dr-staci"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img15} />
          </Link>
        </div>
        {/* <div className="col-lg-4 col-sm-6">
          {" "}
          <Link
            to="/case-study-marthyn/"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image className={CaseStudiesImagesStyle.image} fluid={img17} />
          </Link>
        </div> */}{" "}
        <div className="col-lg-4 col-sm-6">
          <Link
            to="/case-study-adan"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image
              className={CaseStudiesImagesStyle.image}
              fluid={{
                aspectRatio: 0.907258064516129,
                sizes: "(max-width: 537px) 100vw, 537px",
                src: christianadan,
                srcSet: christianadan,
              }}
            />
          </Link>
        </div>{" "}
        <div className="col-lg-4 col-sm-6">
          <Link
            to="/case-study-radha"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image
              className={CaseStudiesImagesStyle.image}
              fluid={{
                aspectRatio: 0.907258064516129,
                sizes: "(max-width: 537px) 100vw, 537px",
                src: rhada,
                srcSet: rhada,
              }}
            />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          <Link
            to="/case-study-dr-katy-jane"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image
              className={CaseStudiesImagesStyle.image}
              fluid={{
                aspectRatio: 0.907258064516129,
                sizes: "(max-width: 537px) 100vw, 537px",
                src: katyjane,
                srcSet: katyjane,
              }}
            />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          <Link
            to="/case-study-dr-liia-ramachandra"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image
              className={CaseStudiesImagesStyle.image}
              fluid={{
                aspectRatio: 0.907258064516129,
                sizes: "(max-width: 537px) 100vw, 537px",
                src: liia,
                srcSet: liia,
              }}
            />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          <Link
            to="/case-study-erin-nance"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image
              className={CaseStudiesImagesStyle.image}
              fluid={{
                aspectRatio: 0.907258064516129,
                sizes: "(max-width: 537px) 100vw, 537px",
                src: erinNance,
                srcSet: erinNance,
              }}
            />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          <Link
            to="/case-study-shadow-cliq"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image
              className={CaseStudiesImagesStyle.image}
              fluid={{
                aspectRatio: 0.907258064516129,
                sizes: "(max-width: 537px) 100vw, 537px",
                src: shadowCliq,
                srcSet: shadowCliq,
              }}
            />
          </Link>
        </div>
        {/* 19.05.2023 */}
        <div className="col-lg-4 col-sm-6">
          <Link
            to="/case-study-katie-couric-and-yahoo"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image
              className={CaseStudiesImagesStyle.image}
              fluid={{
                aspectRatio: 0.907258064516129,
                sizes: "(max-width: 537px) 100vw, 537px",
                src: katie_couric_and_yahoo,
                srcSet: katie_couric_and_yahoo,
              }}
            />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          <Link
            to="/case-study-vanessa-blasic"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image
              className={CaseStudiesImagesStyle.image}
              fluid={{
                aspectRatio: 0.907258064516129,
                sizes: "(max-width: 537px) 100vw, 537px",
                src: vanessa_blasic,
                srcSet: vanessa_blasic,
              }}
            />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          <Link
            to="/case-study-sabine-soto"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image
              className={CaseStudiesImagesStyle.image}
              fluid={{
                aspectRatio: 0.907258064516129,
                sizes: "(max-width: 537px) 100vw, 537px",
                src: sabine_soto,
                srcSet: sabine_soto,
              }}
            />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          <Link
            to="/case-study-get-maine-lobster"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image
              className={CaseStudiesImagesStyle.image}
              fluid={{
                aspectRatio: 0.907258064516129,
                sizes: "(max-width: 537px) 100vw, 537px",
                src: get_maine_lobster,
                srcSet: get_maine_lobster,
              }}
            />
          </Link>
        </div>
        <div className="col-lg-4 col-sm-6">
          <Link
            to="/case-study-john-malecki"
            className={CaseStudiesImagesStyle.image_holder}
          >
            <Image
              className={CaseStudiesImagesStyle.image}
              fluid={{
                aspectRatio: 0.907258064516129,
                sizes: "(max-width: 537px) 100vw, 537px",
                src: john_malecki,
                srcSet: john_malecki,
              }}
            />
          </Link>
        </div>
      </div>
    </section>
  )
}

export default CaseStudiesImages
