import React, { useState } from "react"
import OneMillionFollowersStyle from "./OneMillionFollowersStyle.module.styl"
import Image from "gatsby-image"
import VisibilitySensor from "react-visibility-sensor"
import SplitText from "react-pose-text"

const charPoses = {
  exit: { opacity: 0, y: 0 },
  enter: {
    opacity: 1,
    y: 0,
    delay: ({ charIndex }) => charIndex * 30,
  },
}
function OneMillionFollowersSection({ oneMillionFollowersImage }) {
  const [isVisible, setSectionVisible] = useState(false)

  function onChange() {
    setSectionVisible(!isVisible)
  }
  return (
    <VisibilitySensor
      minTopValue={200}
      partialVisibility={true}
      onChange={onChange}
    >
      <section
        className={OneMillionFollowersStyle.one_million_followers_section}
      >
        <div className={OneMillionFollowersStyle.description}>
          {isVisible === false ? (
            <div className={OneMillionFollowersStyle.case_study}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                Case Study
              </SplitText>
              <span />
            </div>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <h2 className={OneMillionFollowersStyle.title}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                One Million Followers
              </SplitText>
            </h2>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <div className={OneMillionFollowersStyle.objective}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                Objective
              </SplitText>
              <span />
            </div>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <div className={OneMillionFollowersStyle.text}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                In 2018 Brendan Kane released his book “One Million Followers:
                How I built a massive social following in 30 days”
              </SplitText>
            </div>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <div className={OneMillionFollowersStyle.objective}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                Highlights
              </SplitText>
              <span />
            </div>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <ul className={OneMillionFollowersStyle.highlights}>
              <li>
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  Generated
                </SplitText>
                <span>
                  <SplitText
                    initialPose="exit"
                    pose="enter"
                    charPoses={charPoses}
                  >
                    1 million Facebook followers in less than 30 days
                  </SplitText>
                </span>
              </li>

              <li>
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  Consistently generates high engagement on the page with
                  hundreds of thousands of views and tens of thousands of likes
                  and shares
                </SplitText>
              </li>

              <li>
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  Has scaled Facebook audience to build a massive following of
                  more than
                </SplitText>
                <span>
                  {" "}
                  <SplitText
                    initialPose="exit"
                    pose="enter"
                    charPoses={charPoses}
                  >
                    1M followers on Instagram
                  </SplitText>
                </span>
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  (and growing)
                </SplitText>
              </li>

              <li>
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  Secured a book deal with a major publisher and speaking gigs
                  all over the world because of large following
                </SplitText>
              </li>

              <li>
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  Generated over
                </SplitText>
                <span>
                  {" "}
                  <SplitText
                    initialPose="exit"
                    pose="enter"
                    charPoses={charPoses}
                  >
                    15,000 leads in 3 months
                  </SplitText>
                </span>{" "}
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  on Facebook, which helped build email list that helped sell
                  books, workshops, and consulting groups
                </SplitText>
              </li>
            </ul>
          ) : (
            <div />
          )}
        </div>
        <div className={OneMillionFollowersStyle.image_holder}>
          {" "}
          <Image
            className={OneMillionFollowersStyle.image}
            fluid={oneMillionFollowersImage[0].node.childImageSharp.fluid}
          />
        </div>
      </section>
    </VisibilitySensor>
  )
}

export default OneMillionFollowersSection
