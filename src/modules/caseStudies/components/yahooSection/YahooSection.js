import React, { useState } from "react"
import YahooSectionStyle from "./YahooSectionStyle.module.styl"
import Image from "gatsby-image"
import VisibilitySensor from "react-visibility-sensor"
import SplitText from "react-pose-text"

const charPoses = {
  exit: { opacity: 0, y: 0 },
  enter: {
    opacity: 1,
    y: 0,
    delay: ({ charIndex }) => charIndex * 30,
  },
}

function YahooSection({ yahooImage }) {
  const [isVisible, setSectionVisible] = useState(false)

  function onChange() {
    setSectionVisible(!isVisible)
  }
  return (
    <VisibilitySensor
      minTopValue={200}
      partialVisibility={true}
      onChange={onChange}
    >
      <section className={YahooSectionStyle.tylor_nation_section}>
        <div className={YahooSectionStyle.description}>
          {" "}
          {isVisible === false ? (
            <div className={YahooSectionStyle.case_study}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                Case Study
              </SplitText>
              <span />
            </div>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <h2 className={YahooSectionStyle.title}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                Yahoo!
              </SplitText>
            </h2>
          ) : (
            <div />
          )}
          {isVisible === false ? (
            <div className={YahooSectionStyle.objective}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                Objective
              </SplitText>
              <span />
            </div>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <div className={YahooSectionStyle.text}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                Brendan was presented by Yahoo with a challenge to increasing
                content engagement, generating more traffic and higher brand
                engagement
              </SplitText>
            </div>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <div className={YahooSectionStyle.objective}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                Solution
              </SplitText>
              <span />
            </div>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <div className={YahooSectionStyle.text}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                Optimized content marketing strategy, which was developed using
                extensive A/B testing, data mining and social algorithms to
                locate the highest-performing demographic & psychographic
                profiles
              </SplitText>
            </div>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <div className={YahooSectionStyle.objective}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                Results
              </SplitText>
              <span />
            </div>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <ul className={YahooSectionStyle.highlights}>
              <li>
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  Drastically higher engagement & traffic at a significantly
                  lower cost
                </SplitText>
              </li>
              <li>
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  It would have cost Yahoo $6,479,548 to reach the same amount
                  of traffic that we generated during the Q1’16 test for under
                  $20k
                </SplitText>
              </li>
            </ul>
          ) : (
            <div />
          )}
        </div>

        <div className={YahooSectionStyle.image_holder}>
          <Image
            className={YahooSectionStyle.image}
            fluid={yahooImage[0].node.childImageSharp.fluid}
          />
        </div>
      </section>
    </VisibilitySensor>
  )
}

export default YahooSection
