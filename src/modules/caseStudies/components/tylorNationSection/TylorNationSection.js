import React, { useState } from "react"
import TylorNationSectionStyle from "./TylorNationSectionStyle.module.styl"
import Image from "gatsby-image"
import VisibilitySensor from "react-visibility-sensor"
import SplitText from "react-pose-text"

const charPoses = {
  exit: { opacity: 0, y: 0 },
  enter: {
    opacity: 1,
    y: 0,
    delay: ({ charIndex }) => charIndex * 30,
  },
}

function TylorNationSection ({ taylorSwiftImage }) {
  const [isVisible, setSectionVisible] = useState(false)

  function onChange() {
    setSectionVisible(!isVisible)
  }
  return (
    <VisibilitySensor
      minTopValue={200}
      partialVisibility={true}
      onChange={onChange}
    >
      <section className={TylorNationSectionStyle.tylor_nation_section}>
        <div className={TylorNationSectionStyle.description}>
          {" "}
          {isVisible === false ? (
            <div className={TylorNationSectionStyle.case_study}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                Case Study
              </SplitText>{" "}
              <span />
            </div>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <h2 className={TylorNationSectionStyle.title}>
              {" "}
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                Tylor Nation
              </SplitText>
            </h2>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <div className={TylorNationSectionStyle.objective}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                Objective
              </SplitText>
              <span />
            </div>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <div className={TylorNationSectionStyle.text}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                In 2018 Brendan Kane released his book “One Million Followers:
                How I built a massive social following in 30 days”
              </SplitText>
            </div>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <div className={TylorNationSectionStyle.objective}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                Solution
              </SplitText>
              <span />
            </div>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <div className={TylorNationSectionStyle.text}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                A platform that turned any Facebook page into a Taylor Swift fan
                site automatically by pulling the fans profile data through the
                Facebook API and embedding it into an impressive fully
                customizable Taylor Nation fan site.
              </SplitText>
            </div>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <div className={TylorNationSectionStyle.objective}>
              <SplitText initialPose="exit" pose="enter" charPoses={charPoses}>
                Results
              </SplitText>
              <span />
            </div>
          ) : (
            <p />
          )}
          {isVisible === false ? (
            <ul className={TylorNationSectionStyle.highlights}>
              <li>
                {" "}
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  Significantly higher fan engagement & participation
                </SplitText>
              </li>
              <li>
                <span>
                  {" "}
                  <SplitText
                    initialPose="exit"
                    pose="enter"
                    charPoses={charPoses}
                  >
                    30,000+
                  </SplitText>
                </span>{" "}
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  sites built in a few short weeks
                </SplitText>
              </li>
              <li>
                <span>
                  {" "}
                  <SplitText
                    initialPose="exit"
                    pose="enter"
                    charPoses={charPoses}
                  >
                    900,000+
                  </SplitText>
                </span>{" "}
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  votes generated
                </SplitText>
              </li>
            </ul>
          ) : (
            <div />
          )}
        </div>
        <div className={TylorNationSectionStyle.image_holder}>
          {" "}
          <Image
            className={TylorNationSectionStyle.image}
            fluid={taylorSwiftImage[0].node.childImageSharp.fluid}
          />
        </div>
      </section>
    </VisibilitySensor>
  )
}

export default TylorNationSection
