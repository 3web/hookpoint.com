import React, { useState } from "react"
import Image from "gatsby-image"
import RihannaStyle from "./RihannaInitiativeSectionStyle.module.styl"
import VisibilitySensor from "react-visibility-sensor"
import SplitText from "react-pose-text"

const charPoses = {
  exit: { opacity: 0, y: 0 },
  enter: {
    opacity: 1,
    y: 0,
    delay: ({ charIndex }) => charIndex * 30,
  },
}

function RihannaInitiativeSection({ caseStudyRihannaImage }) {
  const [isVisible, setSectionVisible] = useState(false)

  function onChange() {
    setSectionVisible(!isVisible)
  }
  return (
    <VisibilitySensor
      minTopValue={200}
      partialVisibility={true}
      onChange={onChange}
    >
      <section className={RihannaStyle.tylor_nation_section}>
        <div className={RihannaStyle.holder}>
          <div className={RihannaStyle.description}>
            {" "}
            {isVisible === false ? (
              <div className={RihannaStyle.case_study}>
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  Case Study
                </SplitText>{" "}
                <span />
              </div>
            ) : (
              <p />
            )}
            {isVisible === false ? (
              <h2 className={RihannaStyle.title}>
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  Rihana Fan Initiative
                </SplitText>
              </h2>
            ) : (
              <div />
            )}
            {isVisible === false ? (
              <div className={RihannaStyle.objective}>
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  Objective
                </SplitText>
                <span />
              </div>
            ) : (
              <p />
            )}
            {isVisible === false ? (
              <div className={RihannaStyle.text}>
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  Rihanna & her team reached out to Brendan after seeing the
                  success of the Taylor Swift fan initiative with the task of
                  building a viral and engaging fan community
                </SplitText>
              </div>
            ) : (
              <p />
            )}
            {isVisible === false ? (
              <div className={RihannaStyle.objective}>
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  Solution
                </SplitText>
                <span />
              </div>
            ) : (
              <div />
            )}
            {isVisible === false ? (
              <div className={RihannaStyle.text}>
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  Facebook application that automatically merged a fan’s name
                  and photos within Rihanna’s album artwork in order to create a
                  custom Facebook Cover
                </SplitText>
              </div>
            ) : (
              <div />
            )}
            {isVisible === false ? (
              <div className={RihannaStyle.objective}>
                <SplitText
                  initialPose="exit"
                  pose="enter"
                  charPoses={charPoses}
                >
                  Results
                </SplitText>
                <span />
              </div>
            ) : (
              <p />
            )}
            {isVisible === false ? (
              <ul className={RihannaStyle.highlights}>
                <li>
                  <span>
                    {" "}
                    <SplitText
                      initialPose="exit"
                      pose="enter"
                      charPoses={charPoses}
                    >
                      1,000,000+
                    </SplitText>
                  </span>{" "}
                  <SplitText
                    initialPose="exit"
                    pose="enter"
                    charPoses={charPoses}
                  >
                    fans participated in the application
                  </SplitText>
                </li>
                <li>
                  <span>
                    {" "}
                    <SplitText
                      initialPose="exit"
                      pose="enter"
                      charPoses={charPoses}
                    >
                      100,000,000+
                    </SplitText>
                  </span>{" "}
                  <SplitText
                    initialPose="exit"
                    pose="enter"
                    charPoses={charPoses}
                  >
                    people saw the Rihanna Branded Facebook Pages
                  </SplitText>
                </li>
              </ul>
            ) : (
              <div />
            )}
          </div>
        </div>
        <div className={RihannaStyle.image_holder}>
          {" "}
          <Image
            className={RihannaStyle.image}
            fluid={caseStudyRihannaImage[0].node.childImageSharp.fluid}
          />
        </div>
      </section>
    </VisibilitySensor>
  )
}

export default RihannaInitiativeSection
