import React, { useEffect, useState, createRef } from "react"
import lottie from "lottie-web"
import HeroSectionStyle from "./HeroSectionStyle.module.styl"
import { faAngleRight } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Footer from "../../../../common/footer/Footer"
import { Link } from "gatsby"
import { config } from "@fortawesome/fontawesome-svg-core"
import "@fortawesome/fontawesome-svg-core/styles.css"
import animation from "../../../../assets/images/cross_spin"
import Video from "../../../../assets/images/Video_Homepage.mp4"
import SplitText from "react-pose-text"
import VisibilitySensor from "react-visibility-sensor"

config.autoAddCss = false

const charPoses = {
  exit: { opacity: 0, y: 0 },
  enter: {
    opacity: 1,
    y: 0,
    delay: ({ charIndex }) => charIndex * 30,
  },
}

function HeroSection() {
  useEffect(() => {
    const anim = lottie.loadAnimation({
      container: animationContainer.current,
      renderer: "svg",
      loop: true,
      autoplay: true,
      animationData: animation,
    })
    return () => anim.destroy() // optional clean up for unmounting
  }, [])
  let animationContainer = createRef()
  const [isVisible, setSectionVisible] = useState(false)

  function onChange() {
    setSectionVisible(!isVisible)
  }
  return (
    <section className={HeroSectionStyle.hero_section}>
      <div className={HeroSectionStyle.three_seconds_section}>
        <h1 className={HeroSectionStyle.title}>
          You have
          <p className={HeroSectionStyle.number}>
            3 <span>seconds.</span>
          </p>
        </h1>

        <h3>How will you stand out?</h3>

        <Link to="/about" className={HeroSectionStyle.learn_more}>
          Learn More <FontAwesomeIcon icon={faAngleRight} />
        </Link>
      </div>

      <div className={HeroSectionStyle.what_we_do_section}>
        <video autoPlay loop muted playsInline>
          <source src={Video} type="video/mp4" />
        </video>
        <div className={HeroSectionStyle.btn_holder}>
          <Link to="/case-studies" className={HeroSectionStyle.what_we_do}>
            What We Do <FontAwesomeIcon icon={faAngleRight} />
          </Link>
        </div>
        <div className={HeroSectionStyle.cross} ref={animationContainer} />
        <Footer />
      </div>
    </section>
  )
}

export default HeroSection
