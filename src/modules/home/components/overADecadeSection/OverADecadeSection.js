import React, { useEffect, useState, createRef } from "react"
import lottie from "lottie-web"
import OverADecadeSectionStyle from "./OverADecadeSectionStyle.module.styl"
import Image from "gatsby-image"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faAngleRight } from "@fortawesome/free-solid-svg-icons"
import Footer from "../../../../common/footer/Footer"
import { Link } from "gatsby"
import { config } from "@fortawesome/fontawesome-svg-core"
import "@fortawesome/fontawesome-svg-core/styles.css"
import animation from "../../../../assets/images/cross_spin"

config.autoAddCss = false

const charPoses = {
  exit: { opacity: 0, y: 0 },
  enter: {
    opacity: 1,
    y: 0,
    delay: ({ charIndex }) => charIndex * 30,
  },
}

function OverADecadeSection({ overADecadeSectionImage }) {
  const [isVisible, setSectionVisible] = useState(false)
  useEffect(() => {
    const anim = lottie.loadAnimation({
      container: animationContainer.current,
      renderer: "svg",
      loop: true,
      autoplay: true,
      animationData: animation,
    })
    return () => anim.destroy() // optional clean up for unmounting
  }, [])
  let animationContainer = createRef()
  function onChange() {
    setSectionVisible(!isVisible)
  }
  return (
    <section className={OverADecadeSectionStyle.over_a_decade_section}>
      <div className={OverADecadeSectionStyle.image_holder}>
        <Image
          className={OverADecadeSectionStyle.image}
          fluid={overADecadeSectionImage[0].node.childImageSharp.fluid}
        />
        <div
          className={OverADecadeSectionStyle.cross}
          ref={animationContainer}
        />
      </div>

      <div className={OverADecadeSectionStyle.over_a_decade_holder}>
        <h2>Over a decade</h2>

        <div className={OverADecadeSectionStyle.content}>
          of successful project executions for global clients
        </div>

        <div className={OverADecadeSectionStyle.btn_holder}>
          <Link
            to="/case-studies"
            className={OverADecadeSectionStyle.explore_btn}
          >
            Explore Case Studies <FontAwesomeIcon icon={faAngleRight} />
          </Link>
        </div>
      </div>
      <Footer />
    </section>
  )
}

export default OverADecadeSection
