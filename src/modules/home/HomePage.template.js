import React from "react"
import HeroSection from "./components/heroSection/HeroSection"
import BookSectionTemplate from "./components/bookSection/BookSection.template"
import OverADecadeSection from "./components/overADecadeSection/OverADecadeSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import WorkSection from "../caseStudies/components/workSection/WorkSection"

const HomePageTemplate = ({
  taylorSwiftImage,
  homePageBookSectionImage,
  overADecadeSectionImage,
}) => {
  return (
    <>
      <HeroSection taylorSwiftImage={taylorSwiftImage} />
      <BookSectionTemplate
        homePageBookSectionImage={homePageBookSectionImage}
      />
      <OverADecadeSection overADecadeSectionImage={overADecadeSectionImage} />
      <WorkSection />
      <NewsletterSection/>
    </>
  )
}

export default HomePageTemplate
