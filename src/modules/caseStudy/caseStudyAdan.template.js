import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import thumbnail from "../../assets/images/christianadan/cr1.jpg"
import tuple1 from "../../assets/images/christianadan/cr2.webp"
import tuple2 from "../../assets/images/christianadan/cr3.jpeg"

import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyAdan = () => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Cristian Adán"
        titleClassName={s.pierTitle}
        description="Podcaster, author, fitness influencer, and entrepreneur"
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Cristian Adan, a fitness influencer, podcast host, and
                entrepreneur, faced obstacles in growing his podcast.
                Traditional marketing strategies failed to provide real results,
                so Cristian turned to Hook Point for help.
              </p>,
              <p className={s.paragraph}>
                Cristian graduated from university with a Marketing degree but
                struggled to grow his business. He started the Cronosbody
                Podcast, which failed to gain momentum. Cristian discovered
                Brendan Kane's books, One Million Followers and Hook Point,
                which provided the foundation he needed to succeed.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                Cristian faced the challenge of growing his podcast amidst a sea
                of competitors. Traditional marketing tactics failed to provide
                results.
              </p>,
            ],
          },
          {
            elements: [
              <div
                style={{
                  zIndex: 9999,

                  // display: "flex",
                  // justifyContent: "center",
                  // paddingBottom: "100px",
                }}
                className={s.videoHolder}
              >
                <div
                  class="wistia_responsive_padding"
                  style={{
                    padding: "177.78% 0 0 0",
                    position: "relative",
                  }}
                >
                  <div
                    className="wistia_responsive_wrapper"
                    style={{
                      height: "100%",
                      left: 0,
                      position: "absolute",
                      top: 0,
                      width: "100%",
                    }}
                  >
                    <iframe
                      src="https://fast.wistia.net/embed/iframe/c2kq684nar?videoFoam=true"
                      title="Pierfrancesco Conte - Rock Space Video"
                      allow="autoplay; fullscreen"
                      allowtransparency="true"
                      frameborder="0"
                      scrolling="no"
                      className="wistia_embed"
                      name="wistia_embed"
                      msallowfullscreen
                      width="100%"
                      height="100%"
                    ></iframe>
                  </div>
                </div>
                <script
                  src="https://fast.wistia.net/assets/external/E-v1.js"
                  async
                ></script>
              </div>,
            ],
          },
          // {
          //   elements: [<br />, <br />, <br />, <br />],
          // },
        ]}
      />

      <section
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitleWhite}>Solution</h3>
          <p className={s.paragraph}>
            Through Brendan Kane’s books, Cristian learned the principles of
            Hook Points, which he credits for his podcast's success. The
            principles provided Cristian with the guidance he needed to grow his
            podcast and monetize his content. Through Brendan’s coaching,
            Cristian's podcast went from zero to 29 million followers on Spotify
            within months.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                Today, the Cronosbody Podcast has 34 million listeners and is a
                top-three fitness podcast in Latin America. Cristian has
                partnered with global brands like Nestle, GNC, and SmartFit, and
                he has launched an online course that teaches his methodology to
                build the body of a Greek God.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#ededed", "--textClr": "#000" }}
        className={s.flexTuple}
      >
        <div className={s.imageHolder}>
          <img src={tuple2} />
        </div>
        <article style={{ "--paddingX": "80px" }}>
          <h3 className={s.customTitle}>Conclusion</h3>
          <p className={s.paragraph}>
            Hook Point’s methodology provided Cristian with the foundation to
            grow his podcast into a flourishing global business. Cristian's
            story shows that a solid business strategy is necessary to create
            and sustain genuine growth.
          </p>
        </article>
      </section>

      <CaseStudyWorkSection
        next={"/case-study-radha"}
        text="We’ve got more to tell about our work. "
        subText="Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyAdan
