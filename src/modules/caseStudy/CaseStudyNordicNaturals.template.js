import React from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyNordicNaturalsTemplate = ({ image }) => {
  return (
    <React.Fragment>
      {" "}
      <CaseStudyHeaderSection
        title="Nordic Naturals"
        description="A legacy supplement brand expands its reach on social platforms and marketplaces with Hook Points"
        image={image}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Nordic Naturals is a supplement brand that aimed to use social
                media to amplify its brand and propel its products into the mass
                market. However, the company lacked expertise and experience in
                social media, and its internal team struggled to communicate
                their best ideas to executives. That's where Hook Point came in
                - to provide a crash course in social media.
              </p>,
              <p className={s.paragraph}>
                Nordic Naturals started its business in 1995 with a clear
                mission to bring pure, fresh, omega-3 nutrients common in Norway
                to store shelves worldwide. The company grew and stayed true to
                its mission of delivering the world's safest, most effective
                nutrients to help customers take control of their health.
                However, Nordic Naturals remained relatively unknown in the
                supplement field.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                The specific challenges that Nordic Naturals faced were
                revamping a struggling marketing and social team to attract top
                vendors and land key accounts with global distributors. The
                company suspected social media could help them gain traction,
                but they lacked expertise and experience.
              </p>,
              <div
                style={{
                  zIndex: 9999,
                }}
              >
                <div style={{ position: "relative", paddingBottom: "56.25%" }}>
                  <iframe
                    src="https://fast.wistia.net/embed/iframe/0o7e7czish?videoFoam=true"
                    title="Video"
                    allow="autoplay; fullscreen"
                    allowtransparency="true"
                    frameborder="0"
                    scrolling="no"
                    className="wistia_embed"
                    name="wistia_embed"
                    msallowfullscreen
                    width="100%"
                    height="100%"
                    style={{ position: "absolute", inset: "0" }}
                  ></iframe>
                </div>
              </div>,
              <script
                src="https://fast.wistia.net/assets/external/E-v1.js"
                async
              ></script>,
              <br />,
              <br />,
              <br />,
              <br />,
              <h3 className={s.customTitle}>Solution</h3>,
              <p className={s.paragraph}>
                Hook Point saw a massive opportunity in Nordic Naturals and
                immediately integrated social media into the nuts and bolts of
                Nordic Naturals' operations, while preparing the internal team.
                Hook Point taught the creative team the basics of hook points
                and the subconscious communication styles behind consumer
                preferences. Then, Hook Point used its proprietary Communication
                Algorithm model to analyze Nordic Naturals' internal team to
                identify strengths, weaknesses, and opportunities. Finally, Hook
                Point hired a creative team and put them through a rigorous
                course on viral content creation. Together, Hook Point and
                Nordic Naturals developed a tailored strategy to exploit social
                algorithms and amplify the brand. The companies worked together
                to refine their pitch and story for potential partners like CVS,
                Walgreens, Target, and Walmart.
              </p>,
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                The consulting agency achieved huge results, with a massive
                increase in social engagement that helped Nordic Naturals land
                key accounts with Walmart, Target, and other top vendors,
                leading to massive revenue gains. A new YouTube channel received
                10 million views in the first few months. Instagram followers
                increased from 13K to 250K. A 30% revenue increase was
                attributed to new major clients. Nordic Naturals had 2.2X more
                views on Facebook than the leading competitor and 7X more views
                on Instagram than the leading competitor. Hook Point's social
                media and communication expertise helped Nordic Naturals invest
                in social, improve internal communication, and generate
                breakthrough ideas. As a result, the company experienced
                exceptional growth and used its social media success to
                demonstrate to key retailers that they had the best product in
                the market. Want help with your social media strategy? Connect
                1-on-1 with Brendan's team here to see if we are the right fit
                to work together.
              </p>,
              <h3 className={s.customTitle}>Summary</h3>,
              <p className={s.paragraph}>
                Nordic Naturals, a supplement brand, partnered with Hook Point
                to improve its social media presence and attract top vendors.
                Hook Point provided a crash course in social media and used its
                Communication Algorithm model to identify opportunities. The
                consulting agency hired a creative team and developed a tailored
                strategy that led to a massive increase in social engagement,
                revenue gains, and major clients. Nordic Naturals' YouTube
                channel received 10 million views, Instagram followers increased
                from 13K to 250K, and revenue increased by 30%.
              </p>,
            ],
          },
        ]}
      />
      <CaseStudyWorkSection
        next={"/case-study-tanner"}
        text="Explore our work with Nordic Naturals further. Apply to work with us"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyNordicNaturalsTemplate
