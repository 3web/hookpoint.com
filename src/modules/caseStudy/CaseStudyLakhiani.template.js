import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import tuple1 from "../../assets/images/caseStudyLakhiani/tuple1@2x.webp"
import tuple2 from "../../assets/images/caseStudyLakhiani/tuple2@2x.webp"
import path from "../../assets/images/caseStudyLakhiani/Path.png"
import red from "../../assets/images/caseStudyLakhiani/red8@2x.png"
import thumbnail from "../../assets/images/caseStudyLakhiani/main/Ep-138-Vishen-Short-Winners-Banner@2x.webp"

import Cross from "../../assets/images/cross.svg"

import s from "./components/styles/CaseStudyStyle.module.styl"
import SectionsHolder from "./components/SectionsHolder"

const CaseStudyLakhianiTemplate = ({ image }) => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Vishen Lakhiani"
        description="How Hook Point Helped Mindvalley’s Founder Achieve Massive Growth and Expand its Vision to Upgrade Humanity"
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Mindvalley, the world's leading transformational educational
                platform, founded by Vishen Lakhiani, had a vision to upgrade
                humanity. With a 20-year plan in mind, Mindvalley turned to Hook
                Point to boost its expansion and growth.
              </p>,
              <p className={s.paragraph}>
                Vishen Lakhiani, inspired by the vision of a borderless universe
                from Star Trek, founded Mindvalley to bring together the world's
                top educators under one platform to help millions of people
                experience personal transformation. Mindvalley offers online
                courses, books, and events to empower people to “step into their
                greatness.”
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                Vishen faced the challenge of effectively using social media and
                storytelling to drive massive growth in their online audience,
                reach, and revenue. He needed to leverage Hook Points to attract
                and retain a loyal following that would help them monetize their
                offerings and achieve their ambitious vision.
              </p>,
              <br />,
              <br />,
            ],
          },
        ]}
      />
      <section
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitleWhite}>Solution</h3>
          <p className={s.paragraph}>
            To address this challenge, Vishen turned to Hook Point, who trained
            his internal social media and communication department on the
            effective use of storytelling to create engaging and shareable
            content. They developed a comprehensive strategy that focused on
            creating compelling Hook Points in their social media posts, videos,
            and other content to capture the attention of their target audience
            and drive massive growth.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>
      </section>
      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                By implementing the Hook Point strategies, Mindvalley achieved
                impressive results. They increased their video views across
                their platform to 80 million views in just a few months.
                Vishen's Facebook page also saw a significant increase in
                followers, growing from 500,000 to 2.3 million in the same time
                period. This loyal following helped Mindvalley to spread
                important messages and monetize their offerings, driving revenue
                growth and expanding their reach.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#EDEDED", "--textClr": "#000" }}
        className={s.flexTuple}
      >
        <div className={s.imageHolder}>
          <img src={tuple2} />
        </div>
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitle}>Conclusion & Recommendations</h3>
          <p className={s.paragraph}>
            The partnership with Hook Point had a significant impact on Vishen’s
            business. The strategies and tactics implemented by Hook Point
            helped Mindvalley achieve massive growth in their online audience,
            reach, and revenue, enabling them to continue their mission of
            upgrading humanity. Vishen continues to leverage these strategies in
            his ongoing efforts to transform education and create a more
            connected universe.
          </p>
        </article>
      </section>
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                For other companies in the education industry or any industry
                looking to achieve similar results, we recommend considering the
                following recommendations based on Vishen’s experience:
              </p>,
              <p className={s.paragraph}>
                In the fast-paced digital world, engaging and shareable content
                is crucial. Hook Point can help you achieve this.
              </p>,
              <p className={s.paragraph}>
                Emphasize Storytelling: Storytelling can create an emotional
                connection with your audience and build a loyal following. Use
                storytelling techniques in your content to create a compelling
                narrative that resonates with your target audience.
              </p>,
              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-rihanna"}
        text="We’ve got more to tell about our work with Vishen. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyLakhianiTemplate
