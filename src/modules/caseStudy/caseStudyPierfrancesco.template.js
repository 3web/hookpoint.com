import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import thumbnail from "../../assets/images/caseStudyPierfrancesco/thumbnail@2x.webp"
import tuple1 from "../../assets/images/caseStudyPierfrancesco/tuple1@2x.webp"
import tuple2 from "../../assets/images/caseStudyPierfrancesco/tuple2@2x.webp"

import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyPierfrancesco = () => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Pierfrancesco"
        titleClassName={s.pierTitle}
        description="Rock Space"
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Pierfrancesco Conte is an astrophysicist, musician, cinema buff,
                and rock-n-roll historian who had a vision to create a hub for
                his varied interests. Thus, Rock Space was born, which fused
                science and music and was present on almost all social media
                platforms. However, Pierfrancesco faced two significant issues:
                managing his time and lack of direction.
              </p>,
              <p className={s.paragraph}>
                Rock Space was born out of Pierfrancesco's passion, who wanted
                to share his creativity, art, and personality with others. The
                website became quickly popular on social media platforms and
                could be found on YouTube, Instagram, Facebook, Pinterest,
                Spotify, and others.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                The primary challenge Pierfrancesco faced was managing his time
                effectively. He couldn't focus on his full-time job and his
                social media ambition simultaneously. Also, despite being on
                multiple platforms, Rock Space was not taking off due to lack of
                direction and strategy.
              </p>,
            ],
          },
          {
            elements: [
              <div
                style={{
                  zIndex: 9999,

                  // display: "flex",
                  // justifyContent: "center",
                  // paddingBottom: "100px",
                }}
                className={s.videoHolder}
              >
                <div
                  class="wistia_responsive_padding"
                  style={{
                    padding: "177.78% 0 0 0",
                    position: "relative",
                  }}
                >
                  <div
                    className="wistia_responsive_wrapper"
                    style={{
                      height: "100%",
                      left: 0,
                      position: "absolute",
                      top: 0,
                      width: "100%",
                    }}
                  >
                    <iframe
                      src="https://fast.wistia.net/embed/iframe/uuz22x3o6p?videoFoam=true"
                      title="Pierfrancesco Conte - Rock Space Video"
                      allow="autoplay; fullscreen"
                      allowtransparency="true"
                      frameborder="0"
                      scrolling="no"
                      className="wistia_embed"
                      name="wistia_embed"
                      msallowfullscreen
                      width="100%"
                      height="100%"
                    ></iframe>
                  </div>
                </div>
                <script
                  src="https://fast.wistia.net/assets/external/E-v1.js"
                  async
                ></script>
              </div>,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitleWhite}>Solution</h3>
          <p className={s.paragraph}>
            Reading One Million Followers inspired Pierfrancesco to study the
            science behind social media. He tried different strategies to make
            Rock Space stand out but faced challenges initially. However, he
            found the direction he needed after consulting the Hook Point team,
            who provided him with the right solutions and supported him
            throughout the creative process. As a result, Pierfrancesco found
            his voice on social media and the direction he needed.
          </p>
        </article>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                Rock Space engagement skyrocketed to 250K from a few dozen views
                on YouTube. Pierfrancesco could finally share his passions and
                evangelize for rock-n-roll. He now wakes up every morning and
                does what he loves.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#EDEDED", "--textClr": "#000" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "80px" }}>
          <h3 className={s.customTitle}>Conclusion</h3>
          <p className={s.paragraph}>
            Pierfrancesco overcame his obstacles by asking himself if he was
            afraid, and made a promise to himself not to let anything limit him
            from reaching his goals. Hook Point helped Pierfrancesco realize his
            dream successfully. He advised people to approach Hook Point not as
            a product but as a school, where they still have to work hard and
            study to pass the exams.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple2} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Other companies facing similar challenges in the industry can
                benefit from studying the science behind social media and
                seeking help from experts like Hook Point. They can learn how to
                grow their business and overcome the challenges of managing time
                and finding direction on social media platforms.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-stephanie-ackermann"}
        text="We’ve got more to tell about our work. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyPierfrancesco
