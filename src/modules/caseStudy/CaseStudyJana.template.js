import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyJanaTemplate = ({ image }) => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Jana Bartlett"
        description="The art of healing"
        image={image}
      />

      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                In the thriving healing arts industry, Jana Bartlett Alonso
                faced the challenge of effectively reaching her target audience
                and converting them into paying customers. Hook Point helped
                Jana maximize her online presence, leading to exponential growth
                for her School of Integrative Healing.
              </p>,
              <p className={s.paragraph}>
                As a trauma survivor, Jana Bartlett Alonso founded the School of
                Integrative Healing with a vision to teach women how to heal
                from the inside out. Despite growing the business into a
                multi-six-figure venture, she struggled with a demanding
                schedule, diminishing returns, and a weak online presence.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                Jana's primary challenge was her limited reach on social media,
                resulting in weak organic traffic and decreasing sales. Her
                website was in dire need of a makeover to provide an enhanced
                user experience and increase conversions.
              </p>,
            ],
          },

          {
            elements: [
              <div
                style={{
                  zIndex: 9999,

                  // display: "flex",
                  // justifyContent: "center",
                  // paddingBottom: "100px",
                }}
                className={s.videoHolder}
              >
                <div
                  class="wistia_responsive_padding"
                  style={{
                    padding: "177.78% 0 0 0",
                    position: "relative",
                  }}
                >
                  <div
                    className="wistia_responsive_wrapper"
                    style={{
                      height: "100%",
                      left: 0,
                      position: "absolute",
                      top: 0,
                      width: "100%",
                    }}
                  >
                    <iframe
                      src="https://fast.wistia.net/embed/iframe/254w13vq5h?videoFoam=true"
                      title="Pierfrancesco Conte - Rock Space Video"
                      allow="autoplay; fullscreen"
                      allowtransparency="true"
                      frameborder="0"
                      scrolling="no"
                      className="wistia_embed"
                      name="wistia_embed"
                      msallowfullscreen
                      width="100%"
                      height="100%"
                    ></iframe>
                  </div>
                </div>
                <script
                  src="https://fast.wistia.net/assets/external/E-v1.js"
                  async
                ></script>
              </div>,
            ],
          },
          {
            elements: [
              <h3 className={s.customTitle}>Solution</h3>,
              <p className={s.paragraph}>
                Hook Point transformed Jana's online presence by first
                redesigning her website, ensuring a smoother user experience and
                easier access to workshop sign-ups. The agency also optimized
                her Facebook Group, enabling her to provide valuable content and
                host trial challenges for her audience. Additionally, Jana
                implemented Hook Point's Viral Content Engineering and the
                Process Communication Method to produce captivating and
                effective content.
              </p>,
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                The results were astounding, with Jana earning six figures in
                less than a week after implementing Hook Point's strategies. She
                estimates that 80% of the women who participate in the
                immersions enroll in the School for Integrative Health. Jana's
                content strategy quadrupled her views, allowing her to reach
                more women with her life-affirming message.
              </p>,
              <h3 className={s.customTitle}>Conclusion</h3>,
              <p className={s.paragraph}>
                Hook Point's expert strategies and recommendations revitalized
                Jana's online presence, resulting in significant business growth
                and a better work-life balance. They to empower Jana in her
                mission to bring healing and vitality to women's lives.
              </p>,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-mind-valley"}
        text="We’ve got more to tell about our work with Jana. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyJanaTemplate
