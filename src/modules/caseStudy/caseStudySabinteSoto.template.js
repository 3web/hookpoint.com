import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import thumbnail from "../../assets/images/sabine_soto/thumbnail/1618584385090.webp"
import tuple1 from "../../assets/images/sabine_soto/tuple1/132168978_145905737325454_3959051302908472706_n.webp"
import tuple2 from "../../assets/images/sabine_soto/tuple2/t2.webp"

import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudySabineSoto = () => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Sabine Soto"
        titleClassName={s.pierTitle}
        description="Modern Academy"
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Introduction:</h3>,
              <p className={s.paragraph}>
                Modern Academy, a private bilingual K-12 school led by
                entrepreneur Sabine Soto, faced multiple challenges in building
                credibility and attracting students due to its unconventional
                approach to education and the perception of being a "new"
                institution. This case study outlines how consulting agency Hook
                Point worked closely with Sabine to overcome these obstacles and
                establish the school as a prestigious and innovative educational
                leader.
              </p>,
              <div
                // className={s.videoHolder}
                style={{
                  zIndex: 9999,

                  // display: "flex",
                  // justifyContent: "center",
                  paddingBottom: "0px",
                  paddingTop: "0px",
                  paddingLeft: 0,
                  paddingRight: 0,

                  maxWidth: "400px",
                  width: "100%",
                  margin: "0 auto",
                }}
              >
                <div
                  class="wistia_responsive_padding"
                  style={{
                    height: 0,
                    padding: "177% 0 0 0",
                    position: "relative",
                    // maxWidth: 320,
                  }}
                >
                  <div
                    className="wistia_responsive_wrapper"
                    style={{
                      height: "100%",
                      left: 0,
                      position: "absolute",
                      top: 0,
                      width: "100%",
                    }}
                  >
                    <iframe
                      src="https://fast.wistia.net/embed/iframe/ep0vwwmcw1?videoFoam=true"
                      title="Pierfrancesco Conte - Rock Space Video"
                      allow="autoplay; fullscreen"
                      allowtransparency="true"
                      frameborder="0"
                      scrolling="no"
                      className="wistia_embed"
                      name="wistia_embed"
                      msallowfullscreen
                      width="100%"
                      height="100%"
                    ></iframe>
                  </div>
                </div>
                <script
                  src="https://fast.wistia.net/assets/external/E-v1.js"
                  async
                ></script>
              </div>,
              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#ededed", "--textClr": "#000" }}
        className={s.flexTuple}
      >
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>{" "}
        <article style={{ "--paddingX": "80px" }}>
          <h3 className={s.customTitle}>Background:</h3>
          <p className={s.paragraph}>
            Sabine Soto, with her extensive experience in the hospitality
            industry, founded Modern Academy, an innovative school integrating
            problem-solving and project-based learning. With two programs – one
            on-site in Cancun, Mexico, and a worldwide online platform – the
            school offers a comprehensive curriculum, state-of-the-art
            technology, sports, arts, music, and a healthy environment for
            students.
          </p>
        </article>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Challenge:</h3>,
              <p className={s.paragraph}>
                Despite its unique offerings, Modern Academy struggled to
                attract students and forge strategic partnerships with major
                companies. Its relatively short history and the unfamiliarity
                with the Montessori philosophy made it challenging for Sabine to
                reach out to the right audience and convey the value of her
                institution.
              </p>,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitleWhite}>Solution:</h3>
          <p className={s.paragraph}>
            Hook Point provided an all-encompassing strategy to address Modern
            Academy's challenges. Their approach included helping Sabine write a
            book about her educational philosophy, creating an online education
            summit for industry leaders, and utilizing LinkedIn for direct
            outreach to top executives. Furthermore, Hook Point’s strategy
            showed Modern Academy how to establish an internship program,
            implement a compelling social media campaign, and taught Sabine the
            Communication Algorithm to connect with a wider audience.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple2} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Results:</h3>,
              <p className={s.paragraph}>
                Thanks to Hook Point's strategies, Modern Academy achieved
                multiple milestones, including partnering with the EHL
                Hospitality Business School, ranked #1 internationally, and
                securing a book deal for Sabine to promote her vision. The
                school is now set to open an EHL-certified hospitality business
                school in Cancun by 2025, expanding its reach and offerings, and
                increasing visibility.
              </p>,

              <h3 className={s.customTitle}>Conclusion:</h3>,
              <p className={s.paragraph}>
                Hook Point's comprehensive and well-executed strategies played a
                crucial role in positioning Modern Academy as a reputable and
                groundbreaking educational institution. Their methods enabled
                the school to form key alliances, enhance visibility, and
                ultimately grow and thrive. For other educational institutions
                facing similar challenges, adopting strategies similar to those
                implemented by Hook Point can help elevate the institution's
                reputation, forge strategic partnerships, and attain higher
                enrollment rates. Focusing efforts on thought leadership,
                digital outreach, and building relationships with industry
                professionals can contribute to overall success and recognition
                in the competitive educational landscape.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-get-maine-lobster"}
        text="We’ve got more to tell about our work. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudySabineSoto
