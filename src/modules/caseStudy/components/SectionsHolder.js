import React from "react"
import CaseStudyStyle from "./styles/CaseStudyStyle.module.styl"

export default function SectionsHolder({
  sections = [{ title: "", elements: [] }],
  style = {},
}) {
  if (sections.length == 0) {
    return null
  }
  return sections.map(section => {
    return (
      <section style={style} className={CaseStudyStyle.section}>
        {section.title && (
          <h3 className={CaseStudyStyle?.customTitle}>{section?.title}</h3>
        )}
        {section?.elements?.length > 0 &&
          section.elements.map(element => element)}
      </section>
    )
  })
}
