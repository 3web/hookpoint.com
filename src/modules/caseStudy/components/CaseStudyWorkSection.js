import React from "react"
import CaseStudyStyle from "./styles/CaseStudyStyle.module.styl"
import { Link } from "gatsby"

const CaseStudyWorkSection = ({
  text,
  subText,
  next,
  button1text = "Apply Now",
  button2text = "Next Case Study",
}) => {
  return (
    <section className={CaseStudyStyle.case_study_work_section}>
      <div className={CaseStudyStyle.case_study_work_section_description}>
        <h3 className={CaseStudyStyle.case_study_work_section_text}>{text}</h3>
        {subText && <h3 className={CaseStudyStyle.sub_text}>{subText}</h3>}
        <div className={CaseStudyStyle.case_study_buttons_holder}>
          {button1text && (
            <Link to="/apply" className={CaseStudyStyle.apply_now}>
              {button1text}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="5.848"
                height="9.647"
                viewBox="0 0 5.848 9.647"
              >
                <path
                  className="a"
                  d="M4.817,3.5,1.179,0,0,1.229,4.817,5.848l4.83-4.618L8.467,0Z"
                  transform="translate(0 9.647) rotate(-90)"
                />
              </svg>
            </Link>
          )}
          {button2text && (
            <Link className={CaseStudyStyle.next_case_study} to={next}>
              {button2text}
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="5.848"
                height="9.647"
                viewBox="0 0 5.848 9.647"
              >
                <path
                  className="a"
                  d="M4.817,3.5,1.179,0,0,1.229,4.817,5.848l4.83-4.618L8.467,0Z"
                  transform="translate(0 9.647) rotate(-90)"
                />
              </svg>
            </Link>
          )}
        </div>
      </div>
    </section>
  )
}

export default CaseStudyWorkSection
