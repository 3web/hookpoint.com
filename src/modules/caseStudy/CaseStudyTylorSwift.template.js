import React from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import SectionsHolder from "./components/SectionsHolder"
import thumbnail from "../../assets/images/ts/ts-photo-copyright.jpg"
import CaseStudyStyle from "./components/styles/CaseStudyStyle.module.styl"
const CaseStudyTylorSwiftTemplate = ({ image }) => {
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Taylor Swift"
        description="How a Superstar Icon Built a Self-Sustainable Ecosystem of Fans and Revitalized Her Commerce Sales"
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        style={{ backgroundColor: "#EDEDED" }}
        sections={[
          {
            elements: [
              <p className={CaseStudyStyle.paragraph}>
                Taylor Swift, one of the most successful artists of our time,
                faced a challenge when her fan base ballooned into tens of
                millions. Her one-to-one engagement with fans became
                unmanageable and she struggled to create meaningful connections
                with them. Additionally, she was missing out on lucrative retail
                opportunities that her massive following presented. MTV enlisted
                Brendan Kane, founder of Hook Point, to help sharpen her social
                media and commerce strategies.
              </p>,
              <p className={CaseStudyStyle.paragraph}>
                Taylor Swift's priority on building fan relationships started
                early in her career. She responded to every person that reached
                out to her on MySpace at age 15. However, as her fan base grew,
                her one-on-one style quickly became unsustainable. She reached a
                critical point in her career where her popularity was about to
                skyrocket, but her engagement strategy wasn't scalable.
              </p>,
              <h3 className={CaseStudyStyle.customTitle}>Challenge</h3>,
              <p className={CaseStudyStyle.paragraph}>
                The challenge was to scale one-to-one interactions across a
                massive social media fan base to amplify engagement and drive
                commerce sales.
              </p>,
              <h3 className={CaseStudyStyle.customTitle}>Solution</h3>,
              <p className={CaseStudyStyle.paragraph}>
                Brendan and his team understood how to connect at scale and
                recognized the link between e-commerce and engagement. They
                built a technology platform that unified Swift's fan base and
                made it easy for fans to connect with each other. They also
                implemented influencer strategies based on the 90/10 rule and
                significantly increased e-commerce revenue through increased fan
                engagement across social media. They introduced exclusive,
                expensive items like guitars and jackets, which sold out quickly
                on the website.
              </p>,
              <h3 className={CaseStudyStyle.customTitle}>Results</h3>,
              <p className={CaseStudyStyle.paragraph}>
                Using the fans to accelerate Taylor's message was a huge
                success. She achieved her first million-dollar e-commerce month
                on her website and the number of Taylor Swift fan sites grew
                from 30 to over 34,000 in a few weeks.
              </p>,
              <h3 className={CaseStudyStyle.customTitle}>Conclusion</h3>,
              <p className={CaseStudyStyle.paragraph}>
                With Brendan's guidance, Taylor revitalized her social media and
                business strategies. She learned to create self-sustaining
                cycles of fan engagement that translated into increased revenue.
                Brendan's influence left a long-lasting mark on Taylor's widely
                successful social and business strategies.
              </p>,
              <h3 className={CaseStudyStyle.customTitle}>Recommendations</h3>,
              <p className={CaseStudyStyle.paragraph}>
                For companies in the same industry facing similar challenges, it
                is crucial to recognize the link between engagement and
                e-commerce. It is far better to have fans talk about you than it
                is for you to talk about yourself. Companies can take a cue from
                Taylor's strategy and create a self-sustainable ecosystem of
                fans.
              </p>,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        button1text=""
        next={"/case-study-lakhiani"}
        text="There’s more to share about how we helped Taylor Swift."
        subText="Let’s get to know each other."
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyTylorSwiftTemplate
