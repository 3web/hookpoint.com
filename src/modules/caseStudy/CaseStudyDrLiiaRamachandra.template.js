import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import thumbnail from "../../assets/images/CaseStudyDrLiiaRamachandra/banner.webp"
import tuple1 from "../../assets/images/CaseStudyDrLiiaRamachandra/tuple1.webp"
import tuple2 from "../../assets/images/CaseStudyDrLiiaRamachandra/tuple2.webp"

import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyPierfrancesco = () => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Dr. Liia Ramachandra"
        titleClassName={s.pierTitle}
        description="Healing body, mind and spirit!"
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                The skincare and cosmetics industry can be a difficult space to
                navigate, especially for those with specific allergies or
                medical conditions. Dr. Liia Ramachandra, founder of EpiLynx,
                understood this challenge all too well. As someone with a gluten
                allergy, she struggled to find beauty products that wouldn't
                trigger a reaction. That's why she and her husband created
                EpiLynx, a company that offers skincare and makeup free of toxic
                chemicals, gluten, nuts, dairy, and 14 common allergens.
              </p>,
              <p className={s.paragraph}>
                However, disrupting the industry wasn't enough for Dr. Liia. She
                turned to Hook Point to help EpiLynx take the next step.
              </p>,
              <p className={s.paragraph}>
                Dr. Liia's background is in pharmaceuticals and healthcare, not
                marketing or social media. She knew she needed a strategy to
                stand out online and reach more potential customers. Hook Point
                was able to help.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                One of the main challenges EpiLynx faced was standing out in the
                crowded skincare and cosmetics industry. Dr. Liia was also
                concerned about making a significant financial investment and
                time commitment to grow her business.
              </p>,
            ],
          },
          {
            elements: [
              <div
                style={{
                  zIndex: 9999,

                  // display: "flex",
                  // justifyContent: "center",
                  // paddingBottom: "100px",
                }}
                // className={s.videoHolder}
              >
                <div style={{ position: "relative", paddingBottom: "56.25%" }}>
                  <iframe
                    src="https://fast.wistia.net/embed/iframe/3tji5iprxr?videoFoam=true"
                    title="Pierfrancesco Conte - Rock Space Video"
                    allow="autoplay; fullscreen"
                    allowtransparency="true"
                    frameborder="0"
                    scrolling="no"
                    className="wistia_embed"
                    name="wistia_embed"
                    msallowfullscreen
                    width="100%"
                    height="100%"
                    style={{ position: "absolute", inset: "0" }}
                  ></iframe>
                </div>
              </div>,
              <script
                src="https://fast.wistia.net/assets/external/E-v1.js"
                async
              ></script>,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitleWhite}>Solution</h3>
          <p className={s.paragraph}>
            Hook Point worked with Dr. Liia to develop a growth strategy that
            included optimized email marketing, a personalized "skin type" quiz
            to generate leads, and an advertising blueprint to make EpiLynx top
            of mind for cosmetic buyers. The strategy sessions were immensely
            helpful, and the introductions to media and development partners
            benefitted EpiLynx. Those introductions led to new vendors and
            agreements that further boosted their business.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                Through the services of Hook Point, EpiLynx experienced
                substantial growth. The strategy sessions, messaging help, and
                connections made have been incredibly useful. The brainstorming
                sessions and introductions led to new partnerships that have
                expanded Dr. Liia’s business.
              </p>,
              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#EDEDED", "--textClr": "#000" }}
        className={s.flexTuple}
      >
        <div className={s.imageHolder}>
          <img src={tuple2} />
        </div>
        <article style={{ "--paddingX": "80px" }}>
          <h3 className={s.customTitle}>Conclusion</h3>
          <p className={s.paragraph}>
            Thanks to the partnership with Hook Point, in combination with
            high-quality medically clean products, EpiLynx has created a
            committed fan base of customers. From Vegan Volume Mega Mascara to
            Gluten-Free, Hypoallergenic, Vegan Matte Lipstick, EpiLynx has
            cruelty-free, verified products for any skin type.
          </p>
        </article>
      </section>

      <CaseStudyWorkSection
        next={"/case-study-erin-nance/"}
        text="We’ve got more to tell about our work. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyPierfrancesco
