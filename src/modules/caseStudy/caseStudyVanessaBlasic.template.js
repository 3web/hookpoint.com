import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import thumbnail from "../../assets/images/vanessa_blasic/thumbnail/1678553009820@2x.webp"
import tuple1 from "../../assets/images/vanessa_blasic/tuple1/28870284_10101679739465686_3327023762179620864_n@2x.webp"
import tuple2 from "../../assets/images/vanessa_blasic/tuple2/t22.webp"

import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyKatieCouricAndYahoo = () => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Vanessa Blasic"
        titleClassName={s.pierTitle}
        description="The Blonde Vacationist"
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Introduction:</h3>,
              <p className={s.paragraph}>
                Vanessa Blasic, also known as The Blonde Vacationist, owns a
                professional travel, hacking, and coaching business that helps
                people maximize points and earn free vacations. Vanessa turned
                to Hook Point to build a strong foundation for her social media
                presence and introduce her business to a wider audience.
              </p>,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#ededed", "--textClr": "#000" }}
        className={s.flexTuple}
      >
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>{" "}
        <article style={{ "--paddingX": "80px" }}>
          <h3 className={s.customTitle}>Background:</h3>
          <p className={s.paragraph}>
            Vanessa had previously tried various courses and growth hacks to
            expand her social media presence, but none of them seemed to work.
            She was frustrated and skeptical when she approached Hook Point for
            help.
          </p>
        </article>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Challenge:</h3>,
              <p className={s.paragraph}>
                Vanessa's biggest challenge was to create a stronger social
                media presence that could reach a wider audience. She needed to
                learn how to create engaging content and establish her brand
                identity.
              </p>,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitleWhite}>Solution:</h3>
          <p className={s.paragraph}>
            Hook Point provided Vanessa with a deep understanding of social
            media, from scripting to setup, and helped her to create her own
            trend instead of just following others. The consulting agency
            offered a detailed approach, highlighting specific strategies and
            tactics to achieve Vanessa's goals.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple2} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Results:</h3>,
              <p className={s.paragraph}>
                Vanessa implemented the learnings and strategies gained from
                Hook Point and saw a significant increase in her following on
                Instagram and TikTok. She now has more viral videos and is
                preparing to launch a new program in 2023. Vanessa was blown
                away by what she learned from Hook Point and encourages
                entrepreneurs to develop their understanding of how social media
                impacts their businesses.
              </p>,

              <h3 className={s.customTitle}>Conclusion:</h3>,
              <p className={s.paragraph}>
                Hook Point's approach was deeper and more beneficial than other
                courses Vanessa had tried before. It helped her to understand
                social media and create engaging content that resonates with her
                audience. The consulting agency's impact on Vanessa's business
                was significant, and she now has a stronger social media
                presence that attracts more followers and potential clients.
              </p>,
              <h3 className={s.customTitle}>Recommendations:</h3>,
              <p className={s.paragraph}>
                Small businesses, big brands, and individuals should have a
                presence on social media, and Hook Point can teach them the
                actual fundamentals on how to understand the content they’re
                creating so they can excel instead of just chasing trends. If
                you're looking to build a strong social media presence, Hook
                Point is the way to go.
              </p>,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-sabine-soto"}
        text="We’ve got more to tell about our work. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyKatieCouricAndYahoo
