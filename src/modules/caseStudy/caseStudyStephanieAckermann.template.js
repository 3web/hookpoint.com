import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import thumbnail from "../../assets/images/caseStudyStephanieAckermann/thumbnail.webp"
import tuple1 from "../../assets/images/caseStudyStephanieAckermann/tuple1.jpeg"
import tuple2 from "../../assets/images/caseStudyStephanieAckermann/tuple3.webp"

import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyStephanyAckerman = () => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Stephanie Ackermann"
        titleClassName={s.pierTitle}
        description="United Female Gifts - Empowerment through Focused Social Media Strategy"
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                United Female Gifts was founded by Stephanie Ackermann, who
                overcame a harrowing experience with Covid-19 to pursue her
                lifelong dream of empowering women to unlock their hidden magic
                through guided meditation and exploration of their “future happy
                selves.” The business offers a unique niche message about
                spirituality and witchcraft, and Stephanie turned to the Hook
                Point team to build a social media strategy that would enable
                her to reach a wider audience while staying true to her vision.
              </p>,
              <p className={s.paragraph}>
                United Female Gifts offers empowering products and services that
                are intended to help women overcome obstacles in their lives,
                and Stephanie's mission is to enable women to unlock their
                potential through self-discovery. With a unique message in a
                niche market, Stephanie needed a focused digital marketing
                strategy that would help her business reach new audiences.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                Stephanie faced a number of challenges, including how to take
                her message to a wider audience, engage with her customers more
                effectively, and grow her business in a sustainable way. Her
                business was small and did not have a large marketing budget,
                which made it even more difficult to attract and retain new
                customers.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitleWhite}>Solution</h3>
          <p className={s.paragraph}>
            The Hook Point team developed a comprehensive digital marketing
            strategy for Stephanie that included optimizing landing pages,
            increasing the lifetime value of customers, and, most importantly,
            creating bingeable and shareable social media content. The team
            worked closely with Stephanie to develop strategies that aligned
            with her vision while also driving growth and revenue for the
            business.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                Since working with the Hook Point team, United Female Gifts has
                achieved remarkable results. Stephanie describes her business
                growth as “incredible” and credits the Hook Point team with
                helping her develop a clear roadmap for success. Stephanie has
                generated the income she always dreamed of, and she loves the
                intentional and focused plan for growth that the Hook Point team
                has enabled her to develop.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#ededed", "--textClr": "#000" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "80px" }}>
          <h3 className={s.customTitle}>Conclusion</h3>
          <p className={s.paragraph}>
            Through its work with United Female Gifts, the Hook Point team has
            demonstrated its ability to create focused and effective digital
            marketing strategies that help businesses grow and succeed.
            Stephanie's experience demonstrates the power of a clear vision,
            combined with a skilled team that can build and execute effective
            growth strategies.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple2} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                For other businesses in the same industry facing similar
                challenges, we would recommend working with a skilled digital
                marketing team. By developing a focused and effective social
                media strategy, businesses like United Female Gifts can overcome
                their challenges while staying true to their vision and goals.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-planet-home"}
        text="We’ve got more to tell about our work. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyStephanyAckerman
