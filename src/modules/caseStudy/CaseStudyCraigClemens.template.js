import React from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyCraigClemensTemplate = ({ image }) => {
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Craig Clemens"
        description="Hook Points help turn Golden Hippo Media’s Co-Founder into a personal brand"
        image={image}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                The case study explores Craig Clemens' journey in building his
                personal brand through organic social media. As the co-founder
                of Golden Hippo, a highly successful company with over $2
                billion worth of products sold, and one of the world's top
                copywriters, Craig wanted to expand his personal brand to
                connect with celebrities and high-profile individuals for
                potential partnerships. This required him to dive into the world
                of organic social media, which was a different expertise
                compared to his direct response advertising background.
              </p>,
              <p className={s.paragraph}>
                Craig Clemens is the co-founder of Golden Hippo, a company known
                for partnering with high-profile doctors and celebrities for
                product creation. With a strong background in direct response
                advertising, Craig had built an impressive reputation as one of
                the top copywriters in the world. However, he now faced the
                challenge of building his personal brand on social media, a
                venture that greatly differed from his previous experience.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                Although Craig had built an Instagram following of 30,000, he
                desired to start anew with a fresh account and a focused organic
                content strategy. He faced the challenge of transitioning his
                skills from direct response advertising to effectively engaging
                with users through organic social media tactics.
              </p>,
              <h3 className={s.customTitle}>Solution</h3>,
              <p className={s.paragraph}>
                To overcome this challenge, Craig enlisted the help of Hook
                Point, who developed a strategy for Craig and taught him Viral
                Content Engineering. This process showed Craig how to create
                organic content that would resonate with his target audience. By
                tapping into various traffic sources and utilizing his
                storytelling skills, Craig was able to create engaging content
                that would help him stand out on social media.Hook Point
                provided a comprehensive strategy to help Craig better
                understand and utilize viral content engineering techniques.
                They guided him in creating content that would resonate with his
                target audience, drawing from various traffic sources to expand
                his following.
              </p>,
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                Within just a few months, Craig's new Instagram account amassed
                an impressive 800,000 followers. This growth in followers
                enabled him to enter into influential conversations, connect
                with high-profile individuals, and explore potential
                partnerships for Golden Hippo.
              </p>,
              <h3 className={s.customTitle}>Conclusion</h3>,
              <p className={s.paragraph}>
                The collaboration between Craig and Hook Point showcased the
                importance of understanding and adapting to different
                storytelling methods for diverse platforms. By doing so, Craig
                was able to effectively transition from his expertise in direct
                response advertising to organic social media growth, ultimately
                achieving his goal of building a broad and engaged audience.
              </p>,
              <h3 className={s.customTitle}>Recommendations</h3>,
              <p className={s.paragraph}>
                Companies in similar industries or with comparable goals can
                benefit from the lessons learned throughout this case study. By
                recognizing the differences between paid and organic content and
                employing strategies to engage with a wider audience, businesses
                can effectively tap into the power of organic social media to
                expand their reach and foster new partnerships.
              </p>,
              <br />,
              <br />,
            ],
          },
        ]}
      />
      <CaseStudyWorkSection
        next={"/case-study-one-million-followers"}
        text="We’ve got more to tell about our work with Craig. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyCraigClemensTemplate
