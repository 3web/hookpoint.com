import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import thumbnail from "../../assets/images/get_maine_lobster/thumbnail/th2.webp"
import tuple1 from "../../assets/images/get_maine_lobster/tuple1/t12.webp"
import tuple2 from "../../assets/images/get_maine_lobster/tuple2/t22.webp"

import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyKatieCouricAndYahoo = () => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Get Maine Lobster"
        titleClassName={s.pierTitle}
        description="Dock 2 Doorstep"
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Introduction:</h3>,
              <p className={s.paragraph}>
                Get Maine Lobster is a direct-to-consumer seafood delivery
                company that saw explosive growth during Covid. With over 13
                years of experience in the industry and the highest reviews,
                they were already well-positioned to serve their customers.
                However, they wanted to find an edge in the market, discover new
                customers, and explore creative ways to captivate their audience
                and increase conversions. This is where Hook Point came in.
              </p>,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#ededed", "--textClr": "#000" }}
        className={s.flexTuple}
      >
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>{" "}
        <article style={{ "--paddingX": "80px" }}>
          <h3 className={s.customTitle}>Background:</h3>
          <p className={s.paragraph}>
            Get Maine Lobster offers sustainably sourced, hand-selected, and
            harvested Maine Lobster. They have been in business for over 13
            years and have established themselves as a leader in their industry
            with over 14,000 5-star reviews. They were looking for new ways to
            improve their marketing and communication strategies, both
            internally and externally.
          </p>
        </article>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Challenge:</h3>,
              <p className={s.paragraph}>
                Get Maine Lobster faced the challenge of finding an edge in
                their industry and discovering new customers. They wanted to
                create more compelling content and find more creative ways to
                captivate their audience and convert them.
              </p>,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitleWhite}>Solution:</h3>
          <p className={s.paragraph}>
            Hook Point provided an audit of Get Maine Lobster's internal
            creative capabilities and helped them think about their content more
            effectively. They implemented the Process Communication Model and
            hired an in-house graphic designer to overhaul their approach to
            social media content, website content, emails, and texts. Hook
            Point's strategies helped Get Maine Lobster create a distinct voice,
            stand out in a tough industry, and own their market.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple2} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Results:</h3>,
              <p className={s.paragraph}>
                With Hook Point's strategies, Get Maine Lobster saw immediate
                results. Their paid media return on investment increased, and
                their customer acquisition cost went down. They also saw success
                with their blog, "Today's Top Deals," which they turned into a
                special for products. By getting more than one order per
                customer, they were able to invest more and scale rapidly. Their
                organic social media approach has made them much more
                thoughtful, and they're now seeing a lot more engagement.
              </p>,

              <h3 className={s.customTitle}>Conclusion:</h3>,
              <p className={s.paragraph}>
                Thanks to Hook Point's strategies, Get Maine Lobster has not
                only improved their marketing content and internal communication
                but also established a distinct voice in their industry. Get
                Maine Lobster’s CEO said, “Hook Point is like graduate
                school—when your business gets to a certain point, you need to
                look ahead to the next phase and how you can stand out and own
                your market. No one else is offering what Hook Point is. When
                you really want to discover your true voice and project it
                loudly—that’s when you take on Hook Point. It’s a whole
                different way of doing business. There are a lot of voices out
                there. Hook Point can pull out what your real voice is
              </p>,
              <h3 className={s.customTitle}>Recommendations:</h3>,
              <p className={s.paragraph}>
                For companies in the same industry facing similar challenges, we
                recommend exploring the Process Communication Model and
                considering Hook Point's strategies to improve your marketing
                content, internal communication, and overall success. With their
                help, you can develop a distinct voice and stand out in a
                crowded market.
              </p>,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-john-malecki"}
        text="We’ve got more to tell about our work. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyKatieCouricAndYahoo
