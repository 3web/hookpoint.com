import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import thumbnail from "../../assets/images/shadowCliq/banner.jpg"
import tuple1 from "../../assets/images/shadowCliq/tuple1.jpg"
import tuple2 from "../../assets/images/shadowCliq/tuple2.jpg"
import tuple3 from "../../assets/images/shadowCliq/tuple3.jpg"

import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyShadowCliq = () => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Shadow Cliq"
        titleClassName={s.pierTitle}
        description={
          <em style={{ fontWeight: "bold", fontStyle: "oblique" }}>
            “The insight and strategy gained from Hook Point has changed the way
            we look at social media.”
          </em>
        }
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Shadow Cliq, a hard metal rock band, had a desire to amplify
                their social media presence, attract top-tier agents, land a
                high-profile record label contract, and secure invitations to
                major music festivals. However, despite posting frequently, they
                were unable to break through with a viral hit.
              </p>,
              <p className={s.paragraph}>
                Shadow Cliq is a cutting-edge hard metal rock band that sought
                to grow their following and increase their social media
                presence. The band had a loyal following, but they needed to
                reach a larger audience to achieve their goals.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                The specific challenge faced by Shadow Cliq was how to produce a
                viral hit that would expand their reach and increase their
                visibility.
              </p>,
            ],
          },
          {
            elements: [
              <div
                className={s.videoHolder}
                style={{
                  zIndex: 9999,

                  // display: "flex",
                  // justifyContent: "center",
                  paddingBottom: "0px",
                  paddingTop: "0px",
                  paddingLeft: 0,
                  paddingRight: 0,

                  maxWidth: "1159px",
                  width: "100%",
                  margin: "-40px auto 0",
                }}
              >
                <div
                  class="wistia_responsive_padding"
                  style={{
                    padding: "56.25% 0 0 0",
                    position: "relative",
                  }}
                >
                  <div
                    className="wistia_responsive_wrapper"
                    style={{
                      height: "100%",
                      left: 0,
                      position: "absolute",
                      top: 0,
                      width: "100%",
                    }}
                  >
                    <iframe
                      src="https://fast.wistia.net/embed/iframe/wddsoa3uve?videoFoam=true"
                      title="Pierfrancesco Conte - Rock Space Video"
                      allow="autoplay; fullscreen"
                      allowtransparency="true"
                      frameborder="0"
                      scrolling="no"
                      className="wistia_embed"
                      name="wistia_embed"
                      msallowfullscreen
                      width="100%"
                      height="100%"
                    ></iframe>
                  </div>
                </div>
                <script
                  src="https://fast.wistia.net/assets/external/E-v1.js"
                  async
                ></script>
              </div>,
              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        // style={{ "--clr": "#EDEDED", "--textClr": "#000" }}
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitleWhite}>Solution</h3>
          <p className={s.paragraph}>
            Our consulting agency recommended several short-form social media
            formats that have a high potential for going viral. We also educated
            Shadow Cliq on how to analyze those formats and provided the band
            with clear instructions on what to do and what not to do when
            producing those formats. Furthermore, our team conducted detailed
            research and provided insights on Shadow Cliq's existing social
            media strategy and how to pivot for a better chance at a breakout
            viral hit. Our research delved into the specific nuances and
            performance drivers that boosted those formats and helped Shadow
            Cliq stand out.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                Shadow Cliq implemented our recommendations, and as a result,
                they posted a video that garnered an impressive 3 million views.
                This viral video helped them gain a whopping 35,000 new
                followers, which will greatly benefit them in generating more
                concert ticket sales and merchandise purchases.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#EDEDED", "--textClr": "#000" }}
        // style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        {" "}
        <div className={s.imageHolder}>
          <img src={tuple2} />
        </div>
        <article style={{ "--paddingX": "80px" }}>
          <h3 className={s.customTitle}>Conclusion</h3>
          <p className={s.paragraph}>
            We helped Shadow Cliq shift their focus from quantity to quality. By
            teaching them how to produce one viral hit, we helped the band save
            time and get more visibility from a single post. It only takes one
            viral video to launch a stronger social media presence and generate
            more visibility.
          </p>
        </article>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Any artist or entrepreneur can benefit from going viral, as it
                exposes your product or service to a significantly larger
                audience. We teach you the skills for going viral on social
                media, as organic social media growth is one of the most
                effective ways to increase visibility and gain credibility as a
                thought leader in your industry. Increased visibility ultimately
                leads to increased sales because your message is reaching an
                exponentially larger pool of potential customers.
              </p>,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-katie-couric-and-yahoo"}
        text="We’ve got more to tell about our work. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyShadowCliq

{
  /* <div
              style={{
                zIndex: 9999,
                display: "flex",
                justifyContent: "center",
                paddingBottom: "100px",
              }}
            >
              <script
                src="https://fast.wistia.net/embed/iframe/rf7vu916w9.jsonp"
                async
              ></script>
              <script
                src="https://fast.wistia.net/assets/external/E-v1.js"
                async
              ></script>
              <span
                className="wistia_embed wistia_async_rf7vu916w9 popover=true popoverAnimateThumbnail=true"
                style={{
                  display: "inline-block",
                  height: "360px",
                  position: "relative",
                  width: "640px",
                  zIndex: 9999,
                }}
              >
                &nbsp;
              </span>
            </div> */
}
