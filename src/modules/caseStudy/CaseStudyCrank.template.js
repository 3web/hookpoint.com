import React from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyCrankTemplate = ({ image }) => {
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Crank Movie"
        description="Designing Hook Points to elevate an independent action film starring Jason Statham and drive ticket sales on a limited marketing budget"
        image={image}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                When the independent action film Crank was released, it faced a
                significant challenge: a limited marketing budget. However, with
                the help of Hook Point the movie generated an impressive $42
                million at the box office with just a $12 million budget. Hook
                Point's innovative marketing strategies helped Crank succeed.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                Crank was an action film featuring Jason Statham. As an
                independent movie, it faced challenges in a crowded marketplace.
                It had a limited budget for traditional marketing, so it had to
                find alternative ways to generate buzz. The movie was smaller
                than most action films, which made it difficult to attract
                attention.
              </p>,
              <h3 className={s.customTitle}>Solution</h3>,
              <p className={s.paragraph}>
                Hook Point pioneered the first-ever YouTube influencer marketing
                campaign for Crank, leveraging the top YouTube users of the time
                to engage with the film's stars and ignite excitement. This
                audacious and innovative strategy successfully generated
                significant buzz and interest among potential viewers.
              </p>,
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                Thanks to Hook Point's marketing strategies, Crank earned an
                impressive $42 million at the box office, with just a $12
                million budget. This is a staggering result for an independent
                action film of this size. Moreover, the success of Crank led to
                a sequel, which generated additional sales and cemented the
                movie's place in pop culture history.
              </p>,

              <p className={s.paragraph}>
                The movie's unconventional marketing campaign proved that with
                the right plan, a successful outcome is possible, even for
                smaller films.
              </p>,
              <br />,
              <br />,
            ],
          },
        ]}
      />
      <CaseStudyWorkSection
        next={"/case-study-nordic-naturals"}
        text="We’ve got more to share about how Hook Points helped Crank."
        subText=" Connect with us"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyCrankTemplate
