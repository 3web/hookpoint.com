import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import thumbnail from "../../assets/images/john_malecki/thumbnail.jpg"
import tuple1 from "../../assets/images/john_malecki/tuple1.jpg"
import tuple2 from "../../assets/images/john_malecki/tuple2.jpg"

import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"
import useLoadScripts from "../../common/loadScripts"

const CaseStudyJohnMalecki = () => {
  useLoadScripts(["https://www.tiktok.com/embed.js"])

  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        withGradientOnTop
        title="John Malecki"
        titleClassName={s.pierTitle}
        description="Transforming a Woodworking Passion into a Thriving Business"
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Introduction:</h3>,
              <p className={s.paragraph}>
                In the competitive world of woodworking and DIY projects, John
                Malecki, former Pittsburgh Steelers offensive lineman turned
                craftsman, faced the daunting task of establishing his new
                career and sharing his passion with a broader audience. Hook
                Point helped John overcome this obstacle and turn his
                woodworking hobby into a successful business venture through a
                comprehensive social media strategy.
              </p>,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#ededed", "--textClr": "#000" }}
        className={s.flexTuple}
      >
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>{" "}
        <article style={{ "--paddingX": "80px" }}>
          <h3 className={s.customTitle}>Background:</h3>
          <p className={s.paragraph}>
            John Malecki, known for his years with the Pittsburgh Steelers, made
            a significant career transition to pursue his love for woodworking
            and DIY projects. Despite his expertise, he struggled to effectively
            communicate his excitement and capture the attention of others. Hook
            Point recognized the potential in John's talent and assisted him in
            developing a robust social media presence to promote his DIY
            business coaching course, Craft to Career.
          </p>
        </article>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Challenge:</h3>,
              <p className={s.paragraph}>
                The primary challenge John faced was finding ways to engage and
                captivate his target audience, showcasing his expertise and
                craftsmanship in a highly competitive online environment. He
                needed assistance in standing out among the vast sea of DIY
                content creators and reaching a broader audience to promote his
                coaching course effectively.
              </p>,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitleWhite}>Solution:</h3>
          <p className={s.paragraph}>
            Hook Point devised a comprehensive social media strategy tailored to
            John's specific needs. They helped him create compelling organic
            content that resonated with a diverse audience. Leveraging their
            expertise, Hook Point guided John in tapping into viral trends and
            optimizing his content for maximum impact. Furthermore, they
            introduced him to the Process Communication Model (PCM), enabling
            him to connect better with his audience and communicate his message
            effectively.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple2} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Results:</h3>,
              <p className={s.paragraph}>
                Results: The results achieved through Hook Point's social media
                strategy were nothing short of extraordinary. John's social
                media channels, particularly his YouTube and TikTok channels,
                experienced exponential growth. By experimenting with various
                video formats, optimizing thumbnails, and capitalizing on viral
                TikTok woodworking trends, John's average video views
                skyrocketed from 100K to over a million views, reaching a
                significantly wider audience.
              </p>,
              <p className={s.paragraph}>
                In his breakout videos, John strategically incorporated ASMR
                elements, tackled seemingly impossible challenges, and layered
                absurdity in his storytelling. These key performance drivers
                were instrumental in driving the outstanding results he
                achieved.
              </p>,
              <div className={s.tiktokHolder}>
                <section>
                  <blockquote
                    class="tiktok-embed"
                    cite="https://www.tiktok.com/@john_malecki/video/6764361124776398085"
                    data-video-id="6764361124776398085"
                    style={{ maxWidth: 520, minWidth: 300 }}
                  >
                    <section>
                      <a
                        target="_blank"
                        title="@john_malecki"
                        href="https://www.tiktok.com/@john_malecki?refer=embed"
                      >
                        @john_malecki
                      </a>{" "}
                      That final finish on a custom door ! 🔥{" "}
                      <a
                        title="justdiyit"
                        target="_blank"
                        href="https://www.tiktok.com/tag/justdiyit?refer=embed"
                      >
                        #justdiyit
                      </a>{" "}
                      <a
                        title="tiktokpartner"
                        target="_blank"
                        href="https://www.tiktok.com/tag/tiktokpartner?refer=embed"
                      >
                        #tiktokpartner
                      </a>{" "}
                      <a
                        title="diy"
                        target="_blank"
                        href="https://www.tiktok.com/tag/diy?refer=embed"
                      >
                        #diy
                      </a>{" "}
                      <a
                        title="woodworking"
                        target="_blank"
                        href="https://www.tiktok.com/tag/woodworking?refer=embed"
                      >
                        #woodworking
                      </a>{" "}
                      <a
                        target="_blank"
                        title="♬ My House - Flo Rida"
                        href="https://www.tiktok.com/music/My-House-174197883225772032?refer=embed"
                      >
                        ♬ My House - Flo Rida
                      </a>{" "}
                    </section>{" "}
                  </blockquote>
                  <h3 className={s.customTitle}>
                    Before: <span style={{ color: "#FF0301" }}>13K views</span>
                  </h3>
                </section>
                <section>
                  <blockquote
                    class="tiktok-embed"
                    cite="https://www.tiktok.com/@john_malecki/video/7061295600733523246"
                    data-video-id="7061295600733523246"
                    style={{ maxWidth: 520, minWidth: 300 }}
                  >
                    <section>
                      <a
                        target="_blank"
                        title="@john_malecki"
                        href="https://www.tiktok.com/@john_malecki?refer=embed"
                      >
                        @john_malecki
                      </a>{" "}
                      Build a sawhorse in 30 seconds!{" "}
                      <a
                        title="diywithblock"
                        target="_blank"
                        href="https://www.tiktok.com/tag/diywithblock?refer=embed"
                      >
                        #DIYwithBlock
                      </a>{" "}
                      <a
                        title="justdiyit"
                        target="_blank"
                        href="https://www.tiktok.com/tag/justdiyit?refer=embed"
                      >
                        #justdiyit
                      </a>{" "}
                      <a
                        title="todayilearned"
                        target="_blank"
                        href="https://www.tiktok.com/tag/todayilearned?refer=embed"
                      >
                        #todayilearned
                      </a>{" "}
                      <a
                        title="diy"
                        target="_blank"
                        href="https://www.tiktok.com/tag/diy?refer=embed"
                      >
                        #diy
                      </a>{" "}
                      <a
                        target="_blank"
                        title="♬ original sound - John Malecki"
                        href="https://www.tiktok.com/music/original-sound-7061295596933516079?refer=embed"
                      >
                        ♬ original sound - John Malecki
                      </a>{" "}
                    </section>{" "}
                  </blockquote>{" "}
                  <h3 className={s.customTitle}>
                    After: <span style={{ color: "#FF0301" }}>6.4M views</span>
                  </h3>
                </section>
                <script async src="https://www.tiktok.com/embed.js"></script>
              </div>,

              <h3 className={s.customTitle}>Conclusion:</h3>,
              <p className={s.paragraph}>
                John Malecki's success story serves as an inspiration to all
                those pursuing niche hobbies. With the right strategy and
                guidance from Hook Point, John transformed his woodworking
                passion into a thriving online presence. Leveraging a tailored
                social media approach to amplify one's unique skills can help
                anyone reach a broader audience. Hook Point's expertise and
                strategic guidance played a pivotal role in John's remarkable
                journey.
              </p>,
              <h3 className={s.customTitle}>Recommendations:</h3>,
              <p className={s.paragraph}>
                For companies in the woodworking or similar industries, facing
                challenges in reaching a wider audience or monetizing their
                expertise, Hook Point's approach can serve as a valuable
                example. By focusing on organic content creation, leveraging
                viral trends, and employing effective communication models like
                PCM, businesses can maximize their online presence and connect
                with their target audience more authentically. Hook Point's
                expertise positions them as thought leaders and a valuable
                resource for companies seeking to overcome similar challenges
                and achieve remarkable growth in their respective industries.
              </p>,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-tylor-swift"}
        text="We’ve got more to tell about our work. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyJohnMalecki
