import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import thumbnail from "../../assets/images/caseStudyRadha/thumbnail.webp"
import tuple1 from "../../assets/images/caseStudyRadha/tuple1.webp"
import tuple2 from "../../assets/images/caseStudyRadha/tuple2.webp"

import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyRadha = () => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Radha"
        titleClassName={s.pierTitle}
        description="Overcoming Social Anxiety and Building a Career as a Content Creator and Musician
"
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Radha Mestoewa is a standout example of courage and
                determination. Despite a difficult upbringing, she has shown an
                unwavering commitment to building a successful career as a
                content creator, musician and actor. She overcame crippling
                social anxiety and homelessness to become a breakout artist and
                children’s advocate.
              </p>,
              <p className={s.paragraph}>
                Radha has come a long way since her early days in a homeless
                shelter. She invested in herself to create a strong foundation
                for her singing career. Upon seeking clarity and direction,
                Radha reached out to the Hook Point team for support. The team
                provided her with the fundamentals of creating viral content to
                connect with audiences and establish herself as a thought leader
                in the entertainment lifestyle space.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                The main challenge Radha faced was the initial information
                overload when creating content. She was overwhelmed by the
                abundance of “advice” and “secret hacks” on the internet. Radha
                felt that she needed clarity and a roadmap to achieve her goals.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitleWhite}>Solution</h3>
          <p className={s.paragraph}>
            The Hook Point team taught Radha the Viral Content Engineering
            process for employing research, data analysis, and informed ideation
            to create strong content. They provided her strategies to create
            organic content to connect with wider audiences.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                The results are astonishing. Radha recently released her latest
                single “Follow the Warrior” and created her Royal Dreams
                Foundation partnering with organizations committed to supporting
                kids reach their dreams. Radha’s inspiring transformation from
                homeless to breakout musician and actor showcases her talent and
                tenacity. The Hook Point team's guidance enabled Radha to
                develop strong content and establish herself as a leading voice
                in the entertainment lifestyle space.
              </p>,
              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#ededed", "--textClr": "#000" }}
        className={s.flexTuple}
      >
        <div className={s.imageHolder}>
          <img src={tuple2} />
        </div>
        <article style={{ "--paddingX": "80px" }}>
          <h3 className={s.customTitle}>Conclusion</h3>
          <p className={s.paragraph}>
            Radha’s success story demonstrates how investing in oneself can lead
            to personal and professional growth. She overcame social anxiety and
            homelessness, paving the way for a career as a content creator,
            musician, and actor. The Hook Point team provided her with the tools
            and guidance required to create viral content and connect with wider
            audiences.
          </p>
        </article>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                For entrepreneurs and performers in the entertainment space
                looking to establish themselves as thought leaders, the case of
                Radha Mestoewa's transformation from homeless to breakout
                musician and actor is an inspiration. By investing in oneself
                and developing strong content, one can overcome challenges and
                achieve success.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-dr-katy-jane"}
        text="We’ve got more to tell about our work. "
        subText="Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyRadha
