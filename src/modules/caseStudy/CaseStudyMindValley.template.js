import React from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyMindValleyTemplate = ({ image }) => {
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Mindvalley"
        description="Mindvalley's Social Media Strategy Boosted with Hook Point's Viral Content Engineering"
        image={image}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Mindvalley, a leading educational technology company, had a
                respectable social media presence with around 300,000 followers
                on Instagram and Facebook, but their content wasn't yielding
                exceptional results. The company was stuck in the traditional
                marketing strategy, unaware that this approach would not work in
                today’s social landscape. They partnered with Hook Point to
                engineer a new social media strategy.
              </p>,
              <p className={s.paragraph}>
                Mindvalley brings the world’s top educators under a single
                online platform, with thousands of courses and lectures
                available to any Mindvalley subscriber. From the beginning,
                founder Vishen Lakhiani and his team dedicated attention to
                social media because they understood the platforms could push
                their message to a massive audience.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                Mindvalley wanted to move away from traditional marketing
                approaches and toward predictive viral content engineering. They
                needed help to create viral content consistently to connect with
                their viewers and increase their following.
              </p>,
              <div
                style={{
                  zIndex: 9999,
                }}
              >
                <div style={{ position: "relative", paddingBottom: "56.25%" }}>
                  <iframe
                    src="https://fast.wistia.net/embed/iframe/wpfyzol1fc?videoFoam=true"
                    title="Video"
                    allow="autoplay; fullscreen"
                    allowtransparency="true"
                    frameborder="0"
                    scrolling="no"
                    className="wistia_embed"
                    name="wistia_embed"
                    msallowfullscreen
                    width="100%"
                    height="100%"
                    style={{ position: "absolute", inset: "0" }}
                  ></iframe>
                </div>
              </div>,
              <script
                src="https://fast.wistia.net/assets/external/E-v1.js"
                async
              ></script>,
              <br />,
              <br />,
              <br />,
              <h3 className={s.customTitle}>Solution</h3>,
              <p className={s.paragraph}>
                After Brendan Kane, founder of Hook Point, spoke at one of
                Mindvalley’s global retreats, Vishen and his team knew they
                wanted to partner with Hook Point. Hook Point interfaced with
                Mindvalley’s team to create formats and scripts that pushed the
                company’s message–not brand–on social media. The iterative,
                systematic process also showed the team how to use specific
                metrics to define virality and research content formats.
              </p>,
              <h3 className={s.customTitle}>Action Plan</h3>,
              <p className={s.paragraph}>
                The partnership was a masterclass in virality. Hook Point helped
                train Mindvalley's social media team on Hook Points and
                storytelling, engineered social media campaigns on Facebook and
                Instagram, realigned their internal communication strategy,
                brainstormed content formats and scripts, and leveraged
                influencer strategy.
              </p>,
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                The new social media strategy massively boosted Mindvalley's
                followers across platforms and led to a record number of views
                and subscriptions. In six months, Instagram and Facebook
                followers grew from around 70,000 to 900,000, and Facebook
                followers for Vishen Lakhiani, CEO of Mindvalley, increased from
                300,000 to 2.3 million. The company generated 150 million+ views
                to Mindvalley platform and added 400K followers to Mindvalley
                Instagram account.
              </p>,
              <h3 className={s.customTitle}>Conclusion</h3>,
              <p className={s.paragraph}>
                Mindvalley's partnership with Hook Point was one of the most
                formative times for the entire marketing team. The company
                stopped releasing ads and started releasing good content that
                emphasized storytelling and provided value to the audience. Hook
                Point not only grew Mindvalley's business but also taught them a
                system that will allow their team to continue the work even
                after the partnership finishes.
              </p>,
              <h3 className={s.customTitle}>Recommendations</h3>,
              <p className={s.paragraph}>
                For companies in the same industry facing similar challenges,
                Hook Point's systematic approach to viral content engineering
                can be a valuable resource. By partnering with Hook Point,
                companies can create content that connects with their audience,
                generates viral traction, and achieves quantifiable results.
              </p>,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-sahas"}
        subText="Get started here"
        text="Let us tell you more about what we did with MindValley. "
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyMindValleyTemplate
