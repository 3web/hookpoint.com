import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import thumbnail from "../../assets/images/caseStudySahasChopra/image.webp"
import tuple1 from "../../assets/images/caseStudySahasChopra/tuple1.webp"
import tuple2 from "../../assets/images/caseStudySahasChopra/tuple2.webp"

import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudySahasChopra = () => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Sahas Chopra"
        description="How Hook Point Helped Sahas Chopra Scale His Personal Branding Business
"
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Sahas Chopra, a Toronto-based personal branding expert, turned
                to Hook Point to develop a scaling strategy to attract top-tier
                clients, brands, and talent. He needed help driving traffic and
                leads to his website and extending the lifetime value of his
                existing clients.
              </p>,
              <p className={s.paragraph}>
                Sahas is a personal branding expert who helps people amplify
                their message through visual storytelling. He came to Hook Point
                seeking guidance in scaling his business.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                Sahas faced the challenge of attracting high-value clients,
                brands, and talent. He needed to drive traffic and leads to his
                website and extend the lifetime value of his existing clients.
              </p>,
            ],
          },
          {
            elements: [
              <div
                style={{
                  zIndex: 9999,

                  // display: "flex",
                  // justifyContent: "center",
                  // paddingBottom: "100px",
                }}
                className={s.videoHolder}
              >
                <div
                  class="wistia_responsive_padding"
                  style={{
                    padding: "177.78% 0 0 0",
                    position: "relative",
                  }}
                >
                  <div
                    className="wistia_responsive_wrapper"
                    style={{
                      height: "100%",
                      left: 0,
                      position: "absolute",
                      top: 0,
                      width: "100%",
                    }}
                  >
                    <iframe
                      src="https://fast.wistia.net/embed/iframe/ui8mjr8mlw?videoFoam=true"
                      title="SC HOOK POINT REEL Video"
                      allow="autoplay; fullscreen"
                      allowtransparency="true"
                      frameborder="0"
                      scrolling="no"
                      className="wistia_embed"
                      name="wistia_embed"
                      msallowfullscreen
                      width="100%"
                      height="100%"
                    ></iframe>
                  </div>
                </div>
                <script
                  src="https://fast.wistia.net/assets/external/E-v1.js"
                  async
                ></script>
              </div>,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#000000", "--textClr": "#ffffff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitleWhite}>Solution</h3>
          <p className={s.paragraph}>
            The Hook Point team developed a comprehensive strategy to help Sahas
            scale his business quickly and effectively. They provided guidance
            on creating consistent viral content, understanding the psychology
            behind customer behavior, and making effective marketing spends.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                Through practical application of the Hook Point strategies,
                Sahas has seen remarkable results, including increased traffic
                and leads to his website, improved lifetime value of his
                existing clients, and an overall increase in revenue and growth.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#EDEDED", "--textClr": "#000" }}
        className={s.flexTuple}
      >
        <div className={s.imageHolder}>
          <img src={tuple2} />
        </div>
        <article style={{ "--paddingX": "80px" }}>
          <h3 className={s.customTitle}>Conclusion</h3>
          <p className={s.paragraph}>
            Hook Point has been instrumental in helping Sahas scale his personal
            branding business. The team's expertise in creating
            attention-grabbing content and effective marketing strategies has
            helped Sahas achieve his business goals.
          </p>
        </article>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Small business owners, entrepreneurs, solopreneurs, consultants,
                coaches, and anyone who is growth-oriented should take advantage
                of Hook Point's expertise in personal branding and marketing.
                Sahas encourages others to invest in themselves and their
                businesses to achieve the next level of growth.
              </p>,
              <p className={s.paragraph}>
                Overall, Hook Point's guidance and expertise have been
                invaluable to Sahas, and he recommends their services to anyone
                looking to scale their personal brand or business.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-lala-anthony"}
        text="We’ve got more to tell about our work. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudySahasChopra
