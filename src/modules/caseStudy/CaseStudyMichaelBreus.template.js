import React from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyMichaelBreusTemplate = ({ image }) => {
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Michael Breus"
        description="Maximizing Sleep Doctor's Social Media Impact: A Strategic Partnership with Hook Point"
        image={image}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                In the highly competitive healthcare and wellness industry,
                sleep expert Dr. Michael Breus - known as "The Sleep Doctor" -
                faced the challenge of building a social media presence that
                matched his offline success. He required a strategic partner to
                help him understand the intricacies of social media algorithms,
                create compelling content, and maximize audience engagement. The
                collaboration between Dr. Breus and Hook Point allowed him to
                expand his social reach and cement his standing as a thought
                leader in the sleep and wellness space.
              </p>,
              <p className={s.paragraph}>
                Dr. Michael Breus is a renowned sleep expert with a PhD in
                Clinical Psychology and over two decades of experience. He has
                authored multiple books and appeared as a speaker and TV guest
                on numerous occasions. Despite his success offline, Dr. Breus
                struggled to gain traction on social media, a critical aspect in
                securing new engagements, TV appearances, and book deals.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                The main challenges Dr. Breus faced were low social media
                engagement, unclear understanding of social media algorithms, a
                lack of storytelling, and the use of stock images within his
                content. These obstacles were preventing him from expanding his
                online audience, putting at risk opportunities to further grow
                his brand.
              </p>,
              <h3 className={s.customTitle}>Solution</h3>,
              <p className={s.paragraph}>
                Hook Point stepped in as a strategic partner, working closely
                with Dr. Breus to provide a fundamental understanding of social
                media platforms and their functionality. They him storytelling
                strategies and the Viral Content Engineering process to create
                enticing, value-driven content that appealed to a wider
                audience. They supported him in building a strong social media
                presence, specifically on Instagram, by combining a data-driven
                approach with an effective content strategy.
              </p>,
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                Within 60 days of partnering with Hook Point, Dr. Breus saw a
                significant increase in his Instagram followers, reaching
                100,000 in that short time frame. This growth not only expanded
                his audience but also led to higher visibility, more speaking
                engagements, and additional television appearances. As a result
                of this newfound success, Dr. Breus was able to raise his
                speaking fees and even witnessed an acquisition of his brand.
              </p>,
              <h3 className={s.customTitle}>Conclusion</h3>,
              <p className={s.paragraph}>
                The strategic partnership between Dr. Michael Breus and Hook
                Point showcases the power of effective social media strategies
                and storytelling in growing an online presence. By overcoming
                the initial challenges and leveraging key insights, Dr. Breus is
                now positioned as a prominent industry leader with opportunities
                for continued growth and success.
              </p>,
              <h3 className={s.customTitle}>Recommendations</h3>,
              <p className={s.paragraph}>
                For other experts and companies in similar industries facing
                comparable challenges, partnering with Hook Point can be
                invaluable. By understanding the importance of storytelling,
                staying current with social media algorithms, and creating
                engaging content, businesses can maximize their online presence
                and secure their place as thought leaders within their
                respective fields.
              </p>,
              <br />,
              <br />,
            ],
          },
        ]}
      />
      <CaseStudyWorkSection
        next={"/case-study-craig-clemens"}
        text="There’s more to learn about how Hook Points drove opportunity for The Sleep Doctor. "
        subText="Let us tell you more"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyMichaelBreusTemplate
