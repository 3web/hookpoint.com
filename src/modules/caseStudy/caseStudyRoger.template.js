import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import main from "../../assets/images/caseStudyRoger/main@2x.png"
import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"
import tuple1 from "../../assets/images/caseStudyRoger/tuple1/maxresdefadddult@2x.png"
import path from "../../assets/images/caseStudyLakhiani/Path.png"
import tuple2 from "../../assets/images/caseStudyRoger/tuple2/Group 2921@2x.png"
import tuple3 from "../../assets/images/caseStudyRoger/tuple3/Group 2921@2x.png"
import tuple4 from "../../assets/images/caseStudyRoger/tuple4/Group 2921@2x.png"

const CaseStudyRogerTemplate = ({ image }) => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Roger Wakefield"
        description={<>The expert plumber</>}
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: main,
                  srcSet: main,
                },
              },
            },
          },
        ]}
      />

      <SectionsHolder
        sections={[
          {
            elements: [
              <div
                style={{
                  zIndex: 9999,

                  // display: "flex",
                  // justifyContent: "center",
                  // paddingBottom: "100px",
                }}
                className={s.videoHolderWide}
              >
                <div
                  class="wistia_responsive_padding"
                  style={{
                    padding: "50% 0 0 0",
                    position: "relative",
                  }}
                >
                  <div
                    className="wistia_responsive_wrapper"
                    style={{
                      height: "100%",
                      left: 0,
                      position: "absolute",
                      top: 0,
                      width: "100%",
                    }}
                  >
                    <iframe
                      src="https://fast.wistia.net/embed/iframe/17lwpy4mx1?videoFoam=true"
                      title="Pierfrancesco Conte - Rock Space Video"
                      allow="autoplay; fullscreen"
                      allowtransparency="true"
                      frameborder="0"
                      scrolling="no"
                      className="wistia_embed"
                      name="wistia_embed"
                      msallowfullscreen
                      width="100%"
                      height="100%"
                    ></iframe>
                  </div>
                </div>
                <script
                  src="https://fast.wistia.net/assets/external/E-v1.js"
                  async
                ></script>
              </div>,
            ],
          },
          {
            title:
              " PCM literally opened my eyes. The payback has been amazing ",
            elements: [
              <p className={s.paragraph}>
                After reading One Million Followers and Hook Point, “The Expert
                Plumber” Roger Wakefield reached out to the Hook Point team for
                help connecting with his employees, colleagues, and his social
                following.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#EDEDED", "--textClr": "#000" }}
        className={s.flexTuple}
      >
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>
        <article style={{ "--paddingX": "40px" }}>
          <p className={s.paragraph}>
            PCM taught them how to reach different personality types in the
            office through effective language and how to encourage their team’s
            strengths and motivate productivity.
          </p>
        </article>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Not only did PCM help Roger manage the office, it helped him
                expand his online presence and effectively connect with
                audiences in speaking engagements. As a plumber / social media
                influencer / business coach, he encourages people to consider
                careers in the trades and shows them how to use social media
                marketing to grow their business. As Roger says, “once you learn
                how to communicate in the right way, you can get the results
                you’re after.”
              </p>,
              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "80px" }}>
          <p style={{ zIndex: 1 }} className={s.paragraph}>
            Today Roger travels around the country teaching people about the
            value of a career in the trades. On stage, in person, and in his
            marketing materials, Roger explains how PCM is a complement to
            reaching his goals: “PCM has been so good for me. Listening to how
            people talk and thinking I got this. You just have to speak in their
            language.”
          </p>
          <img
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              objectFit: "contain",
              height: "100%",
              width: "100%",
              zIndex: 0,
            }}
            src={path}
          />
        </article>
        <div className={s.imageHolder}>
          <img src={tuple2} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Witnessing the positive effect of PCM in the office as well as
                their explosive growth on YouTube and TikTok, Roger and his
                office manager Amber also decided to become certified PCM
                coaches themselves. They hope to see more electricians, HVAC
                technicians, roofers, carpenters using PCM to reach customers on
                social media.
              </p>,
              <p className={s.paragraph}>
                So next time you need some work done around the house, check
                TikTok. The next roofer/influencer is ready for your call.
              </p>,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#EDEDED", "--textClr": "#000" }}
        className={s.flexTuple}
      >
        <div className={s.imageHolder}>
          <img src={tuple3} />
        </div>
        <article style={{ "--paddingX": "40px" }}>
          <h3 style={{ zIndex: 1 }} className={s.customTitle}>
            Plumber + Social Media Influencer = Unexpected Combination
          </h3>
          <p className={s.paragraph}>
            Roger wants to change that perception. He’s a skilled plumber, a
            successful business owner, and an advocate for tradespeople using
            social media to build their businesses.
          </p>
        </article>
      </section>
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Roger knows from experience how a successful social media
                strategy can either make the phone ring or leave a
                small-business owner waiting for the next customer. He built
                Texas Green Plumbing into a successful business and launched
                “The Trade Talks”–a coaching program that helps aspiring
                tradespeople launch careers from the ground up.
              </p>,
              <br />,
              <br />,

              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "80px" }}>
          <p style={{ zIndex: 1 }} className={s.paragraph}>
            Roger’s initial success grew his business, but he still faced
            management problems familiar to all business owners. As he tried to
            scale his business, he struggled to communicate effectively with his
            team. Additionally, his online training courses weren’t connecting
            with the wider audience he knew could be receptive to his content.
            He turned to Hook Points to learn how to manage his team and also
            create messaging that would resonate with a wider audience.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple4} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <p style={{ zIndex: 1 }} className={s.paragraph}>
                Once Roger had the communication tools in place, his management
                style shifted. Now his team works together to grow his brand and
                his reach is expanding. He’s able to integrate his training and
                message to a new cohort of plumbing professionals and
                entrepreneurs.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-tylor-swift"}
        text="We’ve got more to tell about our work. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyRogerTemplate

// const jsx = (
//   <CaseStudyDescriptionSection
//     title={
//       <h3>
//         <div
//           style={{
//             zIndex: 9999,
//             display: "flex",
//             justifyContent: "center",
//             paddingBottom: "50px",
//           }}
//         >
//           <script
//             src="https://fast.wistia.net/embed/iframe/17lwpy4mx1.jsonp"
//             async
//           ></script>
//           <script
//             src="https://fast.wistia.net/assets/external/E-v1.js"
//             async
//           ></script>
//           <span
//             className="wistia_embed wistia_async_17lwpy4mx1 popover=true popoverAnimateThumbnail=true"
//             style={{
//               display: "inline-block",
//               height: "360px",
//               position: "relative",
//               width: "640px",
//               zIndex: 9999,
//             }}
//           >
//             &nbsp;
//           </span>
//         </div>
//         <span>Not a content problem. A communication problem. </span>
//         <br />
//         <br />
//         Have you heard this one before? It’s not what you said, it’s the way you
//         said it.
//         <br />
//         <br />
//         We’re all too familiar with the struggles and misunderstandings that can
//         arise from poorly chosen words. Even with the best intentions, our ideas
//         can fall flat and we just can’t connect with the people we most want to
//         reach.
//         <br />
//         <br />
//         It might not be a content problem, but a communication problem.
//         <br />
//         <br />
//         Did you know that the majority of the population communicates through
//         fun, facts, and feelings? The Process Communication Model (PCM) is a
//         diagnostic tool that breaks communication styles down into 6 categories:
//         fun, facts, feeling, values, actions, and imagination. Fun, facts, and
//         feelings account for a whopping 75% of the public, which means that
//         messaging that emphasises the top three hits home, but language that
//         neglects the top three flops.
//         {/* <span>
//               {" "}
//               most parents want to do whatever they can to prevent their kids
//               from suffering from a chronic disease, where they might not have
//               given their kids’ cavities a second thought.
//             </span> */}
//         <br />
//         <br />
//         After reading One Million Followers and Hook Point, “The Expert Plumber”
//         Roger Wakefield reached out to the Hook Point team for help connecting
//         with his employees, colleagues, and his social following. He and his
//         business manager of Texas Green Plumbing, Amber Mendoza, enrolled in PCM
//         training, the same communication framework that Pixar uses in all of
//         their scripts and marketing material to connect the world to its films.
//         <br />
//         <br />
//         To date, over 1.4 PCM profiles have been completed and are helping
//         people connect professionally and relationally. How? By helping people
//         increase their connection percentage with core target audiences from 30%
//         to over 75%.
//         <br />
//         <br />
//         <span>The PCM Difference</span>
//         <br />
//         <br />
//         Roger’s communication strengths are action, facts, and values; his
//         business manager Amber’s are action, fun, and feelings. Together this
//         means their management style was strong on getting things done, but they
//         needed to lean on each other for facts and fun. PCM taught them how to
//         reach different personality types in the office through effective
//         language and how to encourage their team’s strengths and motivate
//         productivity.
//         <br />
//         <br />
//         Amber says the work environment has completely changed since she and
//         Roger implemented PCM. “No more stress and tension” as they coach their
//         employees to navigate conflict and work better together. Morale has
//         improved as the team sees their needs being met and everyone is moving
//         in the same direction.
//         <br />
//         <br />
//         <span>
//           “PCM literally opened my eyes. The payback has been amazing”
//         </span>
//         <br />
//         <br />
//         Not only did PCM help Amber manage the office, it helped Roger expand
//         his online presence and effectively connect with audiences in speaking
//         engagements. As a plumber / social media influencer / business coach, he
//         encourages people to consider careers in the trades and shows them how
//         to use social media marketing to grow their business. As Roger says,
//         “once you learn how to communicate in the right way, you can get the
//         results you’re after.”
//         <br />
//         <br />
//         <span>Want to learn more about Roger Wakefield? Keep reading.</span>
//         <br />
//         <br />
//         It’s 3:00 am and you wake up to what sounds like a car backfiring in
//         your house. You rush into the kitchen and are horrified to find that
//         your water pipes have burst in the sub-zero temperatures. As you
//         scramble to find the water shut-off valve you frantically wonder how
//         you’re going to find a plumber at this hour, or even where to look.
//         <br />
//         <br />
//         <span>Plumbing Emergency? Check Instagram</span>
//         <br />
//         <br />
//         And then you remember your Instagram feed and a comic reel describing
//         exactly what you’re going through. And there’s the answer to your
//         emergency–Roger Wakefield–“The Expert Plumber” and social media
//         influencer.
//         <br />
//         <br />
//         Plumber and social media influencer–an unexpected combination, right?
//         Roger wants to change that perception. He’s a skilled plumber, a
//         successful business owner, and an advocate for tradespeople using social
//         media to build their businesses.
//         <br />
//         <br />
//         Roger knows from experience how a successful social media strategy can
//         either make the phone ring or leave a small-business owner waiting for
//         the next customer. He built Texas Green Plumbing into a successful
//         business and launched “The Trade Talks”–a coaching program that helps
//         aspiring tradespeople launch careers from the ground up.
//         <br />
//         <br />
//         However, he grew frustrated spending thousands of dollars on marketing
//         efforts that went nowhere; in fact, their failed ideas lost him
//         business. Realizing these marketing companies didn’t understand his
//         market, he ventured into social media on his own three years ago.
//         <br />
//         <br />
//         <span>
//           “I was sick and tired of marketing companies ripping me off. I just
//           wanted the phone to ring.”
//         </span>
//         <br />
//         <br />
//         “I was sick and tired of marketing companies ripping me off. I just
//         wanted the phone to ring.” Roger signed up for a business conference on
//         using social media strategies and quickly realized he was not only the
//         only plumber there, but the only tradesperson. Literally no one in
//         residential services was learning how to take advantage of social media
//         to reach customers and grow their brands.
//         <br />
//         <br />
//         <span>Opportunity Knocks</span>
//         <br />
//         <br />
//         Resolving to not waste time, Roger launched his YouTube channel, his
//         Instagram, Facebook, and TikTok channels and saw some quick success. Not
//         only did he generate traffic to his own business, he began educating
//         people about the value of a career in the trades–how to start and how to
//         grow their business using the same social media strategies.
//         <br />
//         <br />
//         <span>Growing Pains</span>
//         <br />
//         <br />
//         Roger’s initial success grew his business, but he still faced management
//         problems familiar to all business owners. As he tried to scale his
//         business, he struggled to communicate effectively with his team.
//         Additionally, his online training courses weren’t connecting with the
//         wider audience he knew could be receptive to his content. He turned to
//         PCM training to learn how to manage his team and also create messaging
//         that would resonate with a wider audience.
//         <br />
//         <br />
//         PCM taught him how to craft messaging in the language most people crave.
//         Roger learned how to maximize his communication strength–facts, and
//         develop his weaker areas–fun and feelings.
//         <br />
//         <br />
//         <span>Largest plumbing YouTube channel in the world</span>
//         <br />
//         <br />
//         Today Roger travels around the country teaching people about the value
//         of a career in the trades. On stage, in person, and in his marketing
//         materials, Roger explains how PCM is a complement to reaching his goals:
//         “PCM has been so good for me. Listening to how people talk and thinking
//         I got this. You just have to speak in their language.”
//         <br />
//         <br />
//         Witnessing the positive effect of PCM in the office as well as their
//         explosive growth on YouTube and TikTok, Roger and Amber are even getting
//         certified to become PCM trainers themselves. They hope to see more
//         electricians, HVAC technicians, roofers, carpenters using PCM to reach
//         customers on social media.
//         <br />
//         <br />
//         So next time you need some work done around the house, check TikTok. The
//         next roofer/influencer is ready for your call.
//         <br />
//         <br />
//         <br />
//       </h3>
//     }
//   />
// )
