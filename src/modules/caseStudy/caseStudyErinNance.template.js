import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import thumbnail from "../../assets/images/erinNance/banner.jpg"
import tuple1 from "../../assets/images/erinNance/tuple1.jpg"
import tuple2 from "../../assets/images/erinNance/tuple2.jpg"
import tik from "../../assets/images/erinNance/tik.png"
import Image from "gatsby-image"
import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyErinNance = () => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Dr. Erin Nance"
        titleClassName={s.pierTitle}
        description="Hook Point Helps Hand Surgeon Dr. Erin Nance Gain Social Media Success"
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Dr. Erin Nance is a highly regarded hand surgeon with a thriving
                private practice who wanted to showcase her expertise in reality
                TV through her media company, Hands on Media. She approached
                Hook Point for guidance on social media and increasing
                viewership for her content.
              </p>,
              <p className={s.paragraph}>
                Dr. Nance had no social media account, zero followers, and no
                clue about how to create viral hand surgery content. Despite
                initial difficulties in gaining more than 500 views on her
                videos, her perseverance paved the way for her success.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                Dr. Nance faced the challenge of creating viral hand surgery
                content to increase her visibility on social media platforms.
              </p>,
            ],
          },
          {
            elements: [
              <div
                className={s.videoHolder}
                style={{
                  zIndex: 9999,

                  // display: "flex",
                  // justifyContent: "center",
                  paddingBottom: "0px",
                  paddingTop: "0px",
                  marginTop: "-40px",
                }}
              >
                <div
                  class="wistia_responsive_padding"
                  style={{
                    padding: "177.78% 0 0 0",
                    position: "relative",
                  }}
                >
                  <div
                    className="wistia_responsive_wrapper"
                    style={{
                      height: "100%",
                      left: 0,
                      position: "absolute",
                      top: 0,
                      width: "100%",
                    }}
                  >
                    <iframe
                      src="https://fast.wistia.net/embed/iframe/n5mmgztfkd?videoFoam=true"
                      title="Pierfrancesco Conte - Rock Space Video"
                      allow="autoplay; fullscreen"
                      allowtransparency="true"
                      frameborder="0"
                      scrolling="no"
                      className="wistia_embed"
                      name="wistia_embed"
                      msallowfullscreen
                      width="100%"
                      height="100%"
                    ></iframe>
                  </div>
                </div>
                <script
                  src="https://fast.wistia.net/assets/external/E-v1.js"
                  async
                ></script>
              </div>,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        // style={{ "--clr": "#EDEDED", "--textClr": "#000" }}
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitleWhite}>Solution</h3>
          <p className={s.paragraph}>
            Hook Point provided Dr. Nance with valuable feedback and analysis on
            her content, helping her improve and create viral videos. She
            learned about engagement and the process behind creating successful
            content.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                Within just 7 months, Dr. Nance has amassed over 25 million
                views, and over 200,000 followers, making her a well-known
                figure in the hand surgery community on social media. With
                several videos pulling in over 2.5 million views, her increased
                visibility brought forth new opportunities such as keynote
                speaking invitations.
              </p>,
              <p className={s.paragraph}>
                Dr. Nance attributes her success to the strong foundation laid
                by Hook Point and their valuable feedback. She encourages others
                to experiment and allow themselves sufficient time to learn and
                grow.
              </p>,
              <p className={s.paragraph}>
                Dr. Nance recommends that other companies in the same industry
                facing similar challenges seek feedback and analysis on their
                content to improve and create viral videos. She also recommends
                experimenting and allowing sufficient time to learn and grow.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-shadow-cliq"}
        text="We’ve got more to tell about our work. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyErinNance

{
  /* <div
              style={{
                zIndex: 9999,
                display: "flex",
                justifyContent: "center",
                paddingBottom: "100px",
              }}
            >
              <script
                src="https://fast.wistia.net/embed/iframe/rf7vu916w9.jsonp"
                async
              ></script>
              <script
                src="https://fast.wistia.net/assets/external/E-v1.js"
                async
              ></script>
              <span
                className="wistia_embed wistia_async_rf7vu916w9 popover=true popoverAnimateThumbnail=true"
                style={{
                  display: "inline-block",
                  height: "360px",
                  position: "relative",
                  width: "640px",
                  zIndex: 9999,
                }}
              >
                &nbsp;
              </span>
            </div> */
}
