import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyDrStaciTemplate = ({ image }) => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Doctor Staci"
        description="Can we make dentistry exciting at scale?"
        image={image}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Dr. Staci, a renowned pediatric dentist based in Portland,
                Oregon, faced an intriguing challenge. Despite having
                high-quality educational materials, she struggled to captivate
                people's attention over the issue of cavities in children. Hook
                Point’s innovative approach was able to transform Dr. Staci's
                communication strategy and help her achieve exceptional results.
              </p>,
              <p className={s.paragraph}>
                Dr. Staci is an expert in pediatric dentistry and has dedicated
                her career to combating the world's number one chronic disease
                in children - cavities. Despite cavities causing sleep and
                breathing issues in 9 out of 10 kids, public awareness on the
                topic remains low. Dr. Staci sought our expertise to help her
                engage, educate, and make a real difference.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                Dr. Staci's primary challenge was the dull nature of her subject
                matter, making it difficult to generate interest, scale her
                reach, and ultimately impact children's health. Moreover, she
                needed to find a sustainable revenue source for her compostable
                floss pick business and expand her professional network.
              </p>,

              <div
                style={{
                  zIndex: 9999,
                  display: "flex",
                  justifyContent: "center",
                  paddingBottom: "100px",
                }}
              >
                <script
                  src="https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
                  async
                ></script>
                <script
                  src="https://fast.wistia.com/assets/external/E-v1.js"
                  async
                ></script>
                <span
                  class="wistia_embed wistia_async_l4eb1wk31e popover=true popoverAnimateThumbnail=true"
                  style={{
                    display: "inline-block",
                    height: "640px",
                    position: "relative",
                    width: "360px",
                    zIndex: 9999,
                  }}
                >
                  &nbsp;
                </span>
              </div>,
              <br />,
              <br />,
              <br />,
              <h3 className={s.customTitle}>Solution</h3>,
              <p className={s.paragraph}>
                Hook Point employed a multi-pronged approach to address Dr.
                Staci's challenges:
              </p>,
              <div className={s.list}>
                <ol>
                  <li>
                    Repositioning her mission statement - By reframing her
                    mission from merely discussing cavities to addressing
                    chronic disease in children, we made her message more
                    impactful and difficult to ignore.
                  </li>
                  <li>
                    Restructuring the floss pick business - We introduced a
                    subscription-based "Hygiene Kit," providing Dr. Staci with a
                    sustainable and scalable revenue source.
                  </li>
                  <li>
                    Building professional connections - We helped Dr. Staci
                    engage with influencers in the health and wellness space,
                    private schools, and environmental organizations, enhancing
                    her platform and professional network.
                  </li>
                  <li>
                    Content strategy revamp - We created a content strategy that
                    showcased dramatic real-life cases in children's oral
                    health, making the issue more engaging.
                  </li>
                  <li>
                    Communication analysis and overhaul - By incorporating our
                    communication algorithm, Dr. Staci effectively connected and
                    engaged with a broader audience.
                  </li>
                </ol>
              </div>,
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                Incorporating these Hook Points led to remarkable outcomes for
                Dr. Staci:
              </p>,
              <div className={s.list}>
                <ul>
                  <li>
                    She experienced a surge in client appointments, even
                    requiring her to hire an additional dentist as people began
                    traveling from afar to consult with her.
                  </li>
                  <li>
                    Within a few months, she signed a book publishing deal and
                    launched her patented flosser in the market, significantly
                    raising her profile.
                  </li>
                </ul>
              </div>,
              <h3 className={s.customTitle}>Conclusion</h3>,
              <p className={s.paragraph}>
                Dr. Staci's case study illustrates the power of strategic
                messaging and targeted solutions in overcoming challenges and
                achieving business success. By employing Hook Points, she
                transformed her communication and content, ultimately amplifying
                her impact on children's health.
              </p>,
              <br />,
              <br />,
            ],
          },
        ]}
      />
      <CaseStudyWorkSection
        next={"/case-study-adan"}
        text="We’ve got more to tell about our work with Dr. Staci. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyDrStaciTemplate
