import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import thumbnail from "../../assets/images/katie_couric_and_yahoo/katie-couric-yahoo-news/katie-couric-yahoo-news@2x.webp"
import tuple1 from "../../assets/images/katie_couric_and_yahoo/tuple1/t1.webp"
import tuple2 from "../../assets/images/katie_couric_and_yahoo/tuple2/t22.webp"

import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyKatieCouricAndYahoo = () => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Katie Couric & Yahoo!"
        titleClassName={s.pierTitle}
        description="Hook Point Helps Yahoo! Studios and Katie Couric Thrive on Digital Platforms"
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Introduction:</h3>,
              <p className={s.paragraph}>
                Yahoo! Studios and Katie Couric faced a challenge: how to
                reshape a broadcasting mogul to reach new online audiences
                without alienating a foundational demographic. Couric's team
                found that the tactics that worked well on broadcast media
                weren't yielding the same results online. They turned to Hook
                Point for help.
              </p>,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#ededed", "--textClr": "#000" }}
        className={s.flexTuple}
      >
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>{" "}
        <article style={{ "--paddingX": "80px" }}>
          <h3 className={s.customTitle}>Background:</h3>
          <p className={s.paragraph}>
            Katie Couric has been synonymous with American journalism and
            broadcasting since the 1980s. Couric's segments on NBC's Today Show
            were part of a cherished and practiced routine for millions of
            Americans. When Katie began working as an anchor on Yahoo! News, her
            team struggled to find the right approach to engage new online
            audiences without losing their foundational demographic.
          </p>
        </article>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Challenge:</h3>,
              <p className={s.paragraph}>
                Couric's team faced the specific challenge of finding a way to
                create content that would be successful on digital platforms.
                The traditional broadcast way of doing things wasn't working,
                and they were struggling to find ways to engage audiences
                online.
              </p>,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitleWhite}>Solution:</h3>
          <p className={s.paragraph}>
            Hook Point helped Couric's team by designing storytelling formats to
            engage new audiences across social media. They re-shifted the
            ideation process to focus on storytelling and systematically tested
            clips to post on Facebook. Hook Point brought a level of expertise
            and sophistication that Couric's team needed to broaden their
            consideration of what types of storytelling could help them acquire
            audiences and what tools they could use to bring their content to
            their attention.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple2} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Results:</h3>,
              <p className={s.paragraph}>
                The results were impressive. Hook Point's interventions created
                a virtuous cycle: people started to notice Couric's clips on
                Facebook. With engineered hook points, they grabbed viewers'
                attention and compelled them to go to Yahoo! to watch the entire
                video. Then, the algorithms noticed their popularity and pushed
                their content to more viewers.
              </p>,
              <p className={s.paragraph}>
                The results included a huge engagement increase on Yahoo!,
                monumental cost-savings, and revenue increase. Hook Point worked
                on over 220 interviews, tested 70,000 content variations, and
                saved Yahoo! 31 million dollars in traffic acquisition costs.
                They also generated 1 billion global platform views, and
                Couric's interview with Brandon Stanton, Founder of Humans of
                New York, received 100 million views. In 2016, Katie Couric
                attracted 42% of all Facebook views for Yahoo! Today.
              </p>,
              <h3 className={s.customTitle}>Conclusion:</h3>,
              <p className={s.paragraph}>
                Hook Point's process created a perfect stride for Couric's team,
                and they were all on board to keep the partnership alive. Even
                after Verizon purchased Yahoo! and Couric stepped away from the
                platform, the newscaster and her team partnered with Hook Point
                to establish her brand across digital and social platforms. The
                results were impressive, and Couric learned from Hook Point to
                be nimble and pivot on the spot.
              </p>,
              <h3 className={s.customTitle}>Recommendations:</h3>,
              <p className={s.paragraph}>
                For other companies in the same industry facing similar
                challenges, Hook Point's case study provides valuable insights.
                Companies need to rethink their approach to digital platforms
                and embrace a more three-dimensional way of parsing their
                audience into sub-groups and engaging with each one in a
                tactical, precise way. Companies need to create engineered hook
                points to grab viewers' attention and compel them to watch the
                entire video. Finally, they need to test and iterate their
                content until they find what works best. By following these
                strategies, companies can successfully reach new audiences and
                grow their business.
              </p>,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-vanessa-blasic"}
        text="We’ve got more to tell about our work. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyKatieCouricAndYahoo
