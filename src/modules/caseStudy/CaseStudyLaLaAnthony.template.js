import React from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyLaLaAnthonyTemplate = ({ image }) => {
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="La La Anthony"
        description="A top TV personality launches her new Facebook Watch show with Hook Points"
        image={image}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                In the highly competitive entertainment world, actress and
                entrepreneur La La Anthony faced the challenge of effectively
                utilizing social media to grow her career. Hook Point helped La
                La optimize her social media presence, ultimately boosting her
                visibility and online engagement.
              </p>,
              <p className={s.paragraph}>
                La La Anthony is an American television personality, actress,
                and entrepreneur known for her work as a VJ on MTV's Total
                Request Live and her appearances in various TV shows and movies.
                Social media plays a critical role in her ability to connect
                with her audience and bolster her career.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                Although La La already had a substantial following on Instagram,
                she lacked a deep understanding of social media strategies and
                how to leverage them for her benefit. She needed assistance with
                optimizing her engagement and understanding content partnerships
                and algorithms to accelerate her growth on the platforms.
              </p>,
              <h3 className={s.customTitle}>Solution</h3>,
              <p className={s.paragraph}>
                Hook Point worked closely with La La to conduct tests on her
                content and demonstrate optimal strategies for engagement. They
                taught her the importance of leveraging strategic relationships
                and partnerships around content, using a specific example
                involving ESPN. Hook Point also supported La La in the launch of
                her Facebook Watch show, helping to develop a strategy that
                would ensure its success and extend its reach beyond her social
                channels.
              </p>,
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                With Hook Point's guidance, La La saw a significant increase in
                her daily follower growth, from 3-5,000 followers to 25-30,000
                followers. Their strategic content advice allowed her to drive
                higher engagement on her social channels. Moreover, her Facebook
                Watch show achieved an impressive 2.1 million views on the first
                episode, validating her ability to generate substantial numbers
                with her content.
              </p>,
              <h3 className={s.customTitle}>Conclusion</h3>,
              <p className={s.paragraph}>
                The collaboration with Hook Point proved to be highly beneficial
                for La La Anthony's career, as they provided valuable insights
                and strategies on how to optimize her social media engagement
                and leverage partnerships. The results not only increased her
                visibility and reach, they also positioned her as a strong
                content creator across various platforms.
              </p>,
              <h3 className={s.customTitle}>Recommendations</h3>,
              <p className={s.paragraph}>
                For other companies and individuals in the entertainment
                industry facing similar challenges, working with a knowledgeable
                consulting agency like Hook Point can unlock new opportunities
                for growth and success. Understanding the nuances of
                algorithm-driven platforms, developing strategic content
                partnerships, and crafting compelling stories are all essential
                to thriving in the world of social media.
              </p>,
              <br />,
            ],
          },
        ]}
      />
      <CaseStudyWorkSection
        next={"/case-study-michael-breus"}
        text="Would you like to get more details about how La La succeeded with Hook Points?"
        subText=" We’d love to tell you more"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyLaLaAnthonyTemplate
