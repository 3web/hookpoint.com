import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import thumbnail from "../../assets/images/CaseStudyDrKatyJane/banner.webp"
import tuple1 from "../../assets/images/CaseStudyDrKatyJane/tuple1.webp"
import tuple2 from "../../assets/images/CaseStudyDrKatyJane/tuple2.webp"

import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyPierfrancesco = () => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Dr. Katy Jane"
        titleClassName={s.pierTitle}
        description="Hook Point Helps Dr. Katy Jane Increase Revenue and Reach with Social Media Strategy"
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Dr. Katy Jane is a counseling Bedic astrologer and spiritual
                thought leader who helps women through crucial life decisions.
                She found Hook Point through Brendan Kane’s book One Million
                Followers: How I Built a Massive Social Following in 30 Days.
              </p>,
              <p className={s.paragraph}>
                Dr. Katy Jane operates in the spiritual and counseling fields
                and was looking for a way to increase revenue and reach through
                social media. She had tried various strategies in the past with
                limited success.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                Dr. Katy Jane needed a social media strategy that would help her
                stand out in a crowded field and reach new clients at scale.
              </p>,
            ],
          },
          {
            elements: [
              <div
                style={{
                  zIndex: 9999,

                  // display: "flex",
                  // justifyContent: "center",
                  // paddingBottom: "100px",
                }}
                // className={s.videoHolder}
              >
                <div style={{ position: "relative", paddingBottom: "56.25%" }}>
                  <iframe
                    src="https://fast.wistia.net/embed/iframe/byryafdg94?videoFoam=true"
                    title="Pierfrancesco Conte - Rock Space Video"
                    allow="autoplay; fullscreen"
                    allowtransparency="true"
                    frameborder="0"
                    scrolling="no"
                    className="wistia_embed"
                    name="wistia_embed"
                    msallowfullscreen
                    width="100%"
                    height="100%"
                    style={{ position: "absolute", inset: "0" }}
                  ></iframe>
                </div>
              </div>,
              <script
                src="https://fast.wistia.net/assets/external/E-v1.js"
                async
              ></script>,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitleWhite}>Solution</h3>
          <p className={s.paragraph}>
            Hook Point provided Dr. Katy Jane with an overall strategy to
            increase revenue, build a social media following, and reach an
            audience at scale with a clear and effective message. This included
            an in-depth look at her business strategy and personalized coaching
            from Brendan Kane.
          </p>
          <p className={s.paragraph}></p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                Dr. Katy Jane has seen a significant improvement in her business
                since working with Hook Point. She has increased revenue and
                built a strong social media following. Her clients have noticed
                improvements in her content creation and overall impact.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#EDEDED", "--textClr": "#000" }}
        className={s.flexTuple}
      >
        <div className={s.imageHolder}>
          <img src={tuple2} />
        </div>
        <article style={{ "--paddingX": "80px" }}>
          <h3 className={s.customTitle}>Conclusion</h3>
          <p className={s.paragraph}>
            Hook Point's guidance has had a transformative impact on Dr. Katy
            Jane's business. She highly recommends their services to
            solopreneurs and business leaders looking to grow their businesses.
          </p>
        </article>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Dr. Katy Jane encourages anyone looking to grow as an
                entrepreneur to consider working with Hook Point.
              </p>,

              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-dr-liia-ramachandra"}
        text="We’ve got more to tell about our work. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyPierfrancesco
