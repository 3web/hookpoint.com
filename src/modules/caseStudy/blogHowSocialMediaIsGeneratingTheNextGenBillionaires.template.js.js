import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import thumbnail from "../../assets/images/blogs/social-wealth.jpg"

import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"
import Brendan1on1Section from "../blog/Brendan1on1Section"

const BlogHowSocialMediaIsGeneratingTheNextGenBillionaires = () => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        topHeaderText="Blog"
        title="How social media is generating the next gen billionaires"
        titleClassName={s.blogTitle}
        descriptionClassName={s?.description_text_blog}
        description={
          <>
            The new oil of today is{" "}
            <span style={{ color: "#FF0301" }}>attention</span>
          </>
        }
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                John Rockefeller. Andrew Carnegie. Cornelius Vanderbilt. Mr.
                Beast. Kylie Jenner. Wait...what?
              </p>,
              <p className={s.paragraph}>
                That’s right. At only 25 years old, Kylie Jenner is a
                21st-century corporate titan. She’s minted. Social media
                royalty, a fashion and cosmetics mogul, and the world’s youngest
                self-made billionaire, according to Forbes, Jenner is just
                getting started.
              </p>,
              <p className={s.paragraph}>
                Whereas yesterday’s titans monopolized oil, steel, and
                railroads, Jenner has tapped into an even more precious
                resource: attention.
              </p>,
              <p className={s.paragraph}>
                In{" "}
                <a href="https://www.youtube.com/watch?v=6DCDGSnRDtM">
                  Get Rich in the New Economy
                </a>
                , Alex Hormozi argues that attention is the new oil of today’s
                economy. He explains how social media giants developed their
                brands in the early stages, growing their influence and reach,
                building follower loyalty and trust so that, when they were
                ready to monetize their audience, the results were
                earth-shattering.
              </p>,
              <div
                style={{ paddingLeft: "30px" }}
                className={s.case_study_description_list}
              >
                <ul>
                  <li>
                    Connor McGregor sold whiskey brand Proper No. Twelve for
                    $600 million.
                  </li>
                  <li>The Rock’s Teremana Tequila is worth $4 billion.</li>
                  <li>
                    Mr. Beast is on his way to becoming YouTube’s first
                    billionaire.
                  </li>
                </ul>
              </div>,
            ],
          },
          {
            elements: [
              <div
                style={{
                  zIndex: 9999,

                  // display: "flex",
                  // justifyContent: "center",
                  // paddingBottom: "100px",
                }}
                // className={s.videoHolder}
              >
                <div style={{ position: "relative", paddingBottom: "56.25%" }}>
                  <iframe
                    style={{ position: "absolute", inset: "0" }}
                    width="100%"
                    height="100%"
                    src="https://www.youtube.com/embed/6DCDGSnRDtM"
                    title="YouTube video player"
                    frameBorder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                  ></iframe>
                </div>
              </div>,
            ],
          },
          {
            elements: [
              <p className={s.paragraph}>
                Pick your metaphor–oil gusher, gold strike, mineral
                lode–attention is a limited and scarce resource and everyone's
                racing to stake their claim first. Two obstacles stand in the
                way of exploiting this resource. Actually, 4 billion and one.
              </p>,
              <div
                style={{ paddingLeft: "30px" }}
                className={s.case_study_description_list}
              >
                <ul>
                  <li>
                    4 billion content creators are fighting for the same
                    attention you are.
                  </li>
                  <li>
                    Time is running out to get in on the early stage of the
                    attention economy.
                  </li>
                </ul>
              </div>,
              <p className={s.paragraph}>
                You can overcome the 4 billion competitors by going viral. It
                just takes one piece of content that breaks through the noise to
                start the ball rolling.
              </p>,
              <p className={s.paragraph}>
                Alex Hormozi confessed that the idea of making content and going
                viral seemed like a waste of time until he experienced a mindset
                shift. He realized that the time spent on content creation is
                the input, and the audience is the output. The audience is a
                compounding return on investment, and it scales exponentially as
                reach grows.
              </p>,
              <p className={s.paragraph}>
                The other obstacle, time, is a little more complicated. Mr.
                Beast told{" "}
                <a href="https://www.youtube.com/watch?v=3A8kawxMOcQ">
                  Joe Rogan
                </a>{" "}
                that he spent 5 years of his life obsessing about virality
                before he became successful. He and his buddies (fellow “crazy
                lunatic YouTubers”) spent tens of thousands of hours in daily
                masterminds studying every nuance of viral content.
              </p>,
            ],
          },
          {
            elements: [
              <div
                style={{
                  zIndex: 9999,

                  // display: "flex",
                  // justifyContent: "center",
                  // paddingBottom: "100px",
                }}
                // className={s.videoHolder}
              >
                <div style={{ position: "relative", paddingBottom: "56.25%" }}>
                  <iframe
                    style={{ position: "absolute", inset: "0" }}
                    width="100%"
                    height="100%"
                    src="https://www.youtube.com/embed/3A8kawxMOcQ"
                    title="YouTube video player"
                    frameBorder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen
                  ></iframe>
                </div>
              </div>,
            ],
          },
          {
            elements: [
              <p className={s.paragraph}>
                The crazy lunatics’ hours of sunk costs paid off tremendous
                dividends; however, do you have tens of thousands of hours to
                research and analyze virality?
              </p>,

              <p className={s.paragraph}>
                You might not have 10,000 hours, but you can buy time. You can
                invest in yourself as an early-stage company.
              </p>,
              <p className={s.paragraph}>
                At Hook Point, we’ve put in the time (over 17 years of
                experience) and research to understand how, why, and what goes
                viral. We understand what it takes to grab, hold, and monetize
                the most valuable resource: attention.
              </p>,
              <p className={s.paragraph}>
                Like the geologists and prospectors who study where the next oil
                deposit lies, our content analysts obsess over virality. We
                invest a tremendous amount of our company's research in
                developing the research and insights because we know it's the
                only way to consistently rise above the noise and grab attention
                at scale.
              </p>,
            ],
          },
        ]}
      />
      <Brendan1on1Section />
      <CaseStudyWorkSection
        next={"/case-studies"}
        button2text="View Case Studies"
        text="We’ve got more to tell about our work. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default BlogHowSocialMediaIsGeneratingTheNextGenBillionaires

{
  /* <div
              style={{
                zIndex: 9999,
                display: "flex",
                justifyContent: "center",
                paddingBottom: "100px",
              }}
            >
              <script
                src="https://fast.wistia.net/embed/iframe/rf7vu916w9.jsonp"
                async
              ></script>
              <script
                src="https://fast.wistia.net/assets/external/E-v1.js"
                async
              ></script>
              <span
                className="wistia_embed wistia_async_rf7vu916w9 popover=true popoverAnimateThumbnail=true"
                style={{
                  display: "inline-block",
                  height: "360px",
                  position: "relative",
                  width: "640px",
                  zIndex: 9999,
                }}
              >
                &nbsp;
              </span>
            </div> */
}
