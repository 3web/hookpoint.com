import React, { useEffect } from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import thumbnail from "../../assets/images/caseStudyTanner/thumbnail.jpg"
import tuple1 from "../../assets/images/caseStudyTanner/tuple1.jpeg"
import tuple2 from "../../assets/images/caseStudyTanner/tuple2.jpg"
import tuple3 from "../../assets/images/caseStudyTanner/tuple3.jpg"

import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyTanner = () => {
  useEffect(() => {
    const script1 = document.createElement("script")
    const script2 = document.createElement("script")

    script1.src = "https://fast.wistia.com/embed/medias/l4eb1wk31e.jsonp"
    script2.src = "https://fast.wistia.com/assets/external/E-v1.js"
    document.body.appendChild(script1)
    document.body.appendChild(script2)
  }, [])
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="Tanner Leatherstein"
        titleClassName={s.pierTitle}
        description="Pegai.com"
        image={[
          {
            node: {
              childImageSharp: {
                fluid: {
                  aspectRatio: 0.7575757575757576,
                  sizes: "(max-width: 900px) 100vw, 900px",
                  src: thumbnail,
                  srcSet: thumbnail,
                },
              },
            },
          },
        ]}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Tanner Leatherstein is an incredibly skilled leather craftsman
                who faced the daunting task of competing against established
                luxury brands. But he didn't let that stop him! He approached us
                at Hook Point with a clear goal: to educate people about the
                value of quality crafted leather goods and provide them with an
                unforgettable experience.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                Now, the challenge was to position a small brand as a viable and
                attractive alternative to legacy luxury brands. Tanner and his
                modest team were passionate but lacked experience in social
                media marketing. As a small business owner, Tanner couldn't
                afford to invest in paid advertising to achieve his goals. His
                aim was to achieve organic growth but he didn't know where to
                start.
              </p>,
            ],
          },
          {
            elements: [
              <div
                style={{
                  zIndex: 9999,

                  // display: "flex",
                  // justifyContent: "center",
                  // paddingBottom: "100px",
                }}
                className={s.videoHolder}
              >
                <div
                  class="wistia_responsive_padding"
                  style={{
                    padding: "177.78% 0 0 0",
                    position: "relative",
                  }}
                >
                  <div
                    className="wistia_responsive_wrapper"
                    style={{
                      height: "100%",
                      left: 0,
                      position: "absolute",
                      top: 0,
                      width: "100%",
                    }}
                  >
                    <iframe
                      src="https://fast.wistia.net/embed/iframe/dzxxfkrsdn?videoFoam=true"
                      title="Pierfrancesco Conte - Rock Space Video"
                      allow="autoplay; fullscreen"
                      allowtransparency="true"
                      frameborder="0"
                      scrolling="no"
                      className="wistia_embed"
                      name="wistia_embed"
                      msallowfullscreen
                      width="100%"
                      height="100%"
                    ></iframe>
                  </div>
                </div>
                <script
                  src="https://fast.wistia.net/assets/external/E-v1.js"
                  async
                ></script>
              </div>,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#000", "--textClr": "#fff" }}
        className={s.flexTuple}
      >
        <article style={{ "--paddingX": "80px" }}>
          <h3 className={s.customTitleWhite}>Solution</h3>
          <p className={s.paragraph}>
            That's where we at Hook Point came in. We taught Tanner the core
            concepts of storytelling and authentic marketing using Viral Content
            Engineering. It was so rewarding to see Tanner learn how to tap into
            broad psychological patterns and communication styles to engage a
            large, diverse audience. We worked with Tanner to research creative
            avenues and iteratively test new formats and posts.
          </p>
        </article>
        <div className={s.imageHolder}>
          <img src={tuple1} />
        </div>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                And the results we achieved together were significant,
                sustained, and robust. Tanner's success was not a stroke of
                luck, but rather the result of an understanding of storytelling,
                virality, and human psychology. These insights helped him to
                engage his audience and create content that went viral.
                Sustained social media growth led to a massive boost in brand
                awareness and advocacy that translated to a significant increase
                in online sales. Monthly website traffic increased from 10K to
                100K, and Tanner attracted 700K followers and 35 million organic
                views on TikTok.
              </p>,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <section
        style={{ "--clr": "#EDEDED", "--textClr": "#000" }}
        className={s.flexTuple}
      >
        <div className={s.imageHolder}>
          <img src={tuple2} />
        </div>
        <article style={{ "--paddingX": "40px" }}>
          <h3 className={s.customTitle}>Conclusion and Recommendations</h3>
          <p className={s.paragraph}>
            The key takeaway from this case study is that we at Hook Point
            helped Tanner Leatherstein to make his message stand out with a
            compelling story. Millions of people watched his content, and in
            return, became loyal to his brand. Our strategies translated to a
            huge income boost for Tanner.
          </p>
        </article>
      </section>

      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Our approach can serve as a model for other companies in the
                same industry that face similar challenges. Understanding
                storytelling, virality, and human psychology is crucial to
                creating engaging and effective content on social media.
              </p>,

              <br />,
              <br />,
              <br />,
              <br />,
              <br />,
            ],
          },
        ]}
      />

      <CaseStudyWorkSection
        next={"/case-study-strike-social"}
        text="We’ve got more to tell about our work. "
        subText=" Let’s talk"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyTanner
