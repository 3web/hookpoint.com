import React from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyStrikeSocialTemplate = ({ image }) => {
  return (
    <React.Fragment>
      {" "}
      <CaseStudyHeaderSection
        title="Strike Social"
        description="An upstart social marketing brand rises in a saturated market through Hook Points"
        image={image}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                Strike Social, an emerging social marketing brand, overcame the
                challenges of a saturated market with the help of Hook Point.
                The right strategies and guidance can propel a small startup to
                a multi-million dollar market leader.
              </p>,
              <p className={s.paragraph}>
                Strike Social initially began as a small company with a handful
                of employees, managing $300,000 in media spend for their
                clients. The company operates in the highly competitive social
                marketing industry, offering innovative marketing solutions to a
                wide range of clients. Despite its potential, Strike Social
                struggled to gain traction in the market and required support to
                scale and maximize its performance.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                The main challenge Strike Social faced was breaking through the
                saturated social marketing market and capturing the attention of
                potential clients. To do this, they needed to restructure their
                company for maximum growth and efficiency, while creating strong
                hook points to attract top-level talent, investors, and
                clientele.
              </p>,
              <h3 className={s.customTitle}>Solution</h3>,
              <p className={s.paragraph}>
                Hook Point provided end-to-end support to Strike Social in
                overcoming these challenges. They carefully assessed Strike
                Social's existing business model and operations, and devised
                tailored strategies to help them streamline their processes,
                enhance efficiency, and optimize overall performance. Hook Point
                also assisted the company in developing powerful hook points
                that allowed them to attract high-profile clients and talent.
              </p>,
              <h3 className={s.customTitle}>Results</h3>,
              <p className={s.paragraph}>
                With Hook Point's strategic guidance, Strike Social achieved
                exponential growth and quickly became a market leader. The
                company's media spend increased dramatically from $300,000 to
                managing over $100 million in clients' media spend within a few
                short years. Moreover, Strike Social was able to draw top-level
                talent, investors, and advisors, eventually securing major
                clientele such as Disney, Xbox, and Fox.
              </p>,
              <h3 className={s.customTitle}>Conclusion</h3>,
              <p className={s.paragraph}>
                Strike Social and Hook Point’s partnership demonstrates the
                transformative impact that insightful strategies and expert
                guidance can have on a business. For other companies facing
                similar challenges, it is essential to have a clear
                understanding of your unique selling points and develop strong
                hook points to stand out in the market. Investing in the right
                support will drive growth, attract high-profile clients, and
                position your company as a market leader.
              </p>,
              <br />,
              <br />,
            ],
          },
        ]}
      />
      <CaseStudyWorkSection
        next={"/case-study-mtv"}
        text="Want to find out more about how we drove Strike Social’s business and brand?"
        subText=" Apply to work with us"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyStrikeSocialTemplate
