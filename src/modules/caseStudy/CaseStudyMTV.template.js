import React from "react"
import CaseStudyHeaderSection from "./components/CaseStudyHeaderSection"
import CaseStudyDescriptionSection from "./components/CaseStudyDescriptionSection"
import CaseStudyWorkSection from "./components/CaseStudyWorkSection"
import NewsletterSection from "../newsletter/NewsletterSection"
import SectionsHolder from "./components/SectionsHolder"
import s from "./components/styles/CaseStudyStyle.module.styl"

const CaseStudyMtvTemplate = ({ image }) => {
  return (
    <React.Fragment>
      <CaseStudyHeaderSection
        title="MTV"
        description="An iconic classic turns to Hook Points to reimagine brand, relationships and talent"
        image={image}
      />
      <SectionsHolder
        sections={[
          {
            elements: [
              <p className={s.paragraph}>
                In the fast-paced and competitive entertainment industry, MTV, a
                pioneer in music and television for over three decades, faced
                the challenge of deepening its brand, talent, and business
                relationships. Hook Point's strategic partnership and innovative
                solutions helped MTV strengthen these relationships and solidify
                its position as a market leader.
              </p>,
              <p className={s.paragraph}>
                MTV is a multinational entertainment network that has been
                revolutionizing music and television since its inception in
                1981. Offering a diverse range of content from music videos and
                reality TV shows to documentaries and movies, MTV has
                consistently adapted to the evolving interests of its audience.
                As a global brand with multiple affiliated networks, the company
                operates in a highly competitive industry that demands constant
                innovation, collaboration, and creativity.
              </p>,
              <h3 className={s.customTitle}>Challenge</h3>,
              <p className={s.paragraph}>
                MTV's primary challenge was to reinforce its brand, attract and
                retain top talent, and establish strategic partnerships for
                long-term success. With a rapidly changing media landscape, the
                company had to find innovative ways to deepen its relationships
                with celebrities, musicians, athletes, and other entertainment
                industry stakeholders while differentiating itself from
                competitors.
              </p>,
              <h3 className={s.customTitle}>Solution</h3>,
              <p className={s.paragraph}>
                Hook Point collaborated with MTV to develop a customized
                approach to their challenges. Together, they created
                technology-based Hook Points"to facilitate stronger business
                relationships with premiere talent and industry partners. They
                forged an NBC/MTV joint venture for the extreme sports event
                business DewTour, a strategic alliance with the popular video
                game Rock Band, and a partnership with cutting-edge media
                company Vice. These technology Hook Points were extended to
                other networks such as VH1, CMT, and Comedy Central.
              </p>,
              <h3 className={s.customTitle}>Result</h3>,
              <p className={s.paragraph}>
                The strategic partnerships and technology-driven Hook Points
                resulted in significant gains for MTV and its subsidiaries. The
                joint venture with NBC, the relationship with Rock Band, and the
                partnership with Vice helped MTV reach new audiences, forge
                strong connections with top-tier talent, and diversify its
                content offerings. Ultimately, these strategic moves reinforced
                MTV's reputation as an innovative market leader.
              </p>,
              <h3 className={s.customTitle}>Conclusion</h3>,
              <p className={s.paragraph}>
                By leveraging Hook Point's expertise and innovative approaches,
                MTV managed to deepen its brand, talent, and business
                relationships across multiple platforms. The tailored solutions
                proved invaluable in helping MTV set itself apart in a highly
                competitive industry.
              </p>,
              <br />,
              <br />,
            ],
          },
        ]}
      />
      <CaseStudyWorkSection
        next={"/case-study-jana-bartlett"}
        text="If you want to dive in deeper about our work with MTV, apply to work with us"
      />
      <NewsletterSection />
    </React.Fragment>
  )
}

export default CaseStudyMtvTemplate
